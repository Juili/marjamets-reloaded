package marjametsa_reloaded;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

enum State{
	AGGRESSIVE, RETREAT;
}

public class Squirrel extends Creature {
	
	private State state_;
	private int range_;
	private int retreatCounter_;
	
	public Squirrel() {
		state_ = State.AGGRESSIVE;
		range_ = 6;
		retreatCounter_ = 0;
	}
	
	public void retreat(){
		retreatCounter_ = 5;
		state_ = State.RETREAT;
	}
	
	@Override
	public boolean act(LevelScheduler LS) {
		if(retreatCounter_ == 0){
			state_ = State.AGGRESSIVE;
		}
		
		while (movesLeft_ > 0) {			
			// get Laura's coordinates
			int LauraX = LS.getLauraX();
			int LauraY = LS.getLauraY();

			// Check if Laura is next to you and state is aggressive
			if ((LauraX == coordinateX_ + 1 && LauraY == coordinateY_ && state_ == State.AGGRESSIVE)
					|| (LauraX == coordinateX_ - 1 && LauraY == coordinateY_)
					|| (LauraY == coordinateY_ + 1 && LauraX == coordinateX_) 
					|| (LauraY == coordinateY_ - 1 && LauraX == coordinateX_)){
				// -----HIT LAURA-----
				Laura.getLaura().reduceHealth(strength_);
				--movesLeft_;
			}
			
			//check if laura in range
			if(Math.sqrt(Math.pow((Laura.getLaura().getX() - coordinateX_), 2) + 
					Math.pow((Laura.getLaura().getY() - coordinateY_), 2)) <= range_){
				
				ArrayList<Direction> moveDirections = new ArrayList<Direction>();
				
				// check directions

				if(state_ == State.AGGRESSIVE){
					//Move towards laura
					if (LauraX > coordinateX_) {
						moveDirections.add(Direction.RIGHT);
					}

					if (LauraX < coordinateX_) {
						moveDirections.add(Direction.LEFT);
					}

					if (LauraY < coordinateY_) {
						moveDirections.add(Direction.UP);
					}

					if (LauraY > coordinateY_) {
						moveDirections.add(Direction.DOWN);
					}
				}
				
				else{
					//Move away from laura
					if (LauraX > coordinateX_) {
						moveDirections.add(Direction.LEFT);
					}

					if (LauraX < coordinateX_) {
						moveDirections.add(Direction.RIGHT);
					}

					if (LauraY < coordinateY_) {
						moveDirections.add(Direction.DOWN);
					}

					if (LauraY > coordinateY_) {
						moveDirections.add(Direction.UP);
					}
				}
				
				long seed = System.nanoTime();
				Collections.shuffle(moveDirections, new Random(seed));

				// check possible direction to move
				Direction moveDecision = Direction.NOWHERE;

				while (moveDirections.size() > 0) {
					if (moveDirections.get(0) == Direction.UP) {
						if (LS.ableToMove(coordinateX_, coordinateY_ - 1)) {
							moveDecision = Direction.UP;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.DOWN) {
						if (LS.ableToMove(coordinateX_, coordinateY_ + 1)) {
							moveDecision = Direction.DOWN;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.LEFT) {
						if (LS.ableToMove(coordinateX_ - 1, coordinateY_)) {
							moveDecision = Direction.LEFT;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.RIGHT) {
						if (LS.ableToMove(coordinateX_ + 1, coordinateY_)) {
							moveDecision = Direction.RIGHT;
							break;
						}
					}

					moveDirections.remove(0);
				}

				// If no possible directions to move, creature stays where it
				// is,
				// and function returns true
				if (moveDirections.size() == 0) {
					--movesLeft_;
					
					if(retreatCounter_ > 0){
						--retreatCounter_;
					}
					
					return true;
				}

				// remove creature from old coordinate
				if (!LS.removeCreature(coordinateX_, coordinateY_)) {
					return false;
				}

				// move to new direction
				this.move(moveDecision);

				// add creature to new coordinate
				LS.addCreature(coordinateX_, coordinateY_, this);

				// Reduce movesleft
				--movesLeft_;
				
				// Reduce retreatCounter
				if(retreatCounter_ > 0){
					--retreatCounter_;
				}
			}
			
			else{
				//move to random direction //TODO
				
				--movesLeft_;
			}
		}
		return true;
	}
	
	@Override
	public int reduceHealth(int damage) {
		int reducedAmount = damage - defense_;
		if(reducedAmount < 0){
			reducedAmount = 0;
		}
		
		health_ -= reducedAmount;
		retreat();
		
		if (health_ < 0) {
			health_ = 0;
		}
		
		return health_;
	}
}
