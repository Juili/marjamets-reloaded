package marjametsa_reloaded;

public class Potion extends Item {
	
	private int healingValue_;
	
	public Potion(int id, String name, int healingValue) {
		super.id_ = id;
		super.name_ = name;
		healingValue_ = healingValue;
	}
	
	public int getHealingValue() {
		return healingValue_;
	}
}
