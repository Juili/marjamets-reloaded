package marjametsa_reloaded;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class JPanelWithBackground extends JPanel { 

	private static final long serialVersionUID = 1L;
	protected Image imageOrg_ = null; 
	protected Image image_ = null;
 
	public JPanelWithBackground(Image i) { 
		imageOrg_ = i; 
		image_ = i; 
		setOpaque(false); 
	}
	
	public void paint(Graphics g) { 
		if (image_ != null) g.drawImage(image_, 0, 0, null); 
		super.paint(g); 
	}
	
	public void setBackground(Image i) {
		imageOrg_ = image_;
		image_ = i;
		setOpaque(false);
		repaint();
	}
	
	public Image getImage(){
		return image_;
	}
}
