package marjametsa_reloaded;

public class CreatureType extends Type {
	
	protected int health_;
	protected int maxHealth_;
	protected int speed_;
	protected int strength_;
	protected int defense_;
	protected int coordinateX_;
	protected int coordinateY_;
	protected String name_;
	
	public CreatureType() {
		
	}
	
	public int getX() {
		return coordinateX_;
	}
	
	public int getY() {
		return coordinateY_;
	}
	
	public int getSpeed() {
		return speed_;
	}
	
	public int getHealth() {
		return health_;
	}
	
	public int getStrength() {
		return strength_;
	}
	
	public int getDefense() {
		return defense_;
	}
	
	public int getMaxHealth() {
		return maxHealth_;
	}
	
	public String getName() {
		return name_;
	}
	
	public void setHealth(int health) {
		health_ = health;
	}
	
	public void setMaxHealth(int maxHealth) {
		maxHealth_ = maxHealth;
	}
	
	public void setSpeed(int speed) {
		speed_ = speed;
	}
	
	public void setStrength(int strength) {
		strength_ = strength;
	}
	
	public void setDefense(int defense) {
		defense_ = defense;
	}
	
	public void setX(int x) {
		coordinateX_ = x;
	}
	
	public void setY(int y) {
		coordinateY_ = y;
	}
	
	public void setName(String name) {
		name_ = name;
	}
}
