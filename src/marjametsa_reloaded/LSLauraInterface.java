package marjametsa_reloaded;

public interface LSLauraInterface {
	public Coordinate getCoordinate(int x, int y);
	public boolean addCreature(int x, int y, Creature cre);
	public boolean removeCreature(int x, int y);
	public int countCreatures();
	public Coordinate coordinateAtDirection(int x, int y, Direction dir);
	public int getMaxX();
	public int getMaxY();
}
