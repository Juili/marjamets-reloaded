package marjametsa_reloaded;

public interface GUICoordinateInterface {
	public void drawCoordinateBackground(String id, int x, int y);
	public void drawCreatureStats(String id, int health, int maxHealth, 
									int strength, int defense, int speed);
	public void drawStackCoordinate(String id, int x, int y);
	public void drawItemStats(String id, String name, int healingValue); //Potion
	public void drawItemStats(String id, String name, int damage, boolean cutsWood); //Weapon
	public void drawShotgunDetail(String id, int rotationsToRight, int x, int y);
}
