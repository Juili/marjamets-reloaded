package marjametsa_reloaded;

import java.util.Vector;

public interface GUILauraInterface {
	public void drawItemInUse(int id);
	public void drawItems(Vector<Integer> ids);
	public void drawLauraStats(int health, int maxHealth, int strength, int defense, int speed);
	public void drawItemStats(String id, String name, int healingValue); //Potion
	public void drawItemStats(String id, String name, int damage, boolean cutsWood); //Weapon
	public void undrawShotgunDetail(Vector<Coordinate> spread);
}
