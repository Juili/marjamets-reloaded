package marjametsa_reloaded;

enum Terrain {
	GRASS, DIRT
}

enum Obstacle {
	EMPTY, TREE, BOULDER, EXIT
}

enum Detail {
	EMPTY, BLOOD
}

public class Coordinate {
	private Terrain terrain_;
	private Obstacle obstacle_;
	private Detail detail_;
	private Creature creature_;
	private Item item_;
	private int x_;
	private int y_;
	private GUICoordinateInterface GUI_;
	
	//Default builder which only takes coordinates as parameters
	public Coordinate(int x, int y){
		
		//Default ground if no parameters
		terrain_ = Terrain.GRASS;
		obstacle_ = Obstacle.EMPTY;
		detail_ = Detail.EMPTY;
		
		//Coordinates from parameters
		x_ = x;
		y_ = y;
	}
	
	//Another builder for defining terrain
	public Coordinate(int x, int y, Terrain ter) {
		
		//Ground and obstacle from parameters
		terrain_ = ter;
		obstacle_ = Obstacle.EMPTY;
		detail_ = Detail.EMPTY;
		
		//Coordinates from parameters
		x_ = x;
		y_ = y;
	}
	
	public int getX() {
		return x_;
	}
	
	public int getY() {
		return y_;
	}
	
	//-----Get-Functions-----
	public Terrain getTerrain() {
		return terrain_;
	}
	
	public Obstacle getObstacle() {
		return obstacle_;
	}
	
	public boolean isObstacle(){
		if(obstacle_ == Obstacle.EMPTY){
			return false;
		}
		return true;
	}
	
	public Detail getDetail() {
		return detail_;
	}
	
	public Creature getCreature() {
		return creature_;
	}
	
	public Item getItem() {
		return item_;
	}
	
	//-----Set-Functions-----
	public void setTerrain(Terrain ter) {
		terrain_ = ter;
	}
	
	public void setObstacle(Obstacle obs) {
		obstacle_ = obs;
	}
	
	public void setDetail(Detail det) {
		detail_ = det;
	}
	
	public void drawAll(){
		drawBackground();
		drawObstacle();
		drawDetail();
		drawItem();
		drawLaura(Laura.getLaura().getFacingDirection());
		drawCreature();
	}
	
	public void drawBackground() {
		String id = "-1";
		if (terrain_ == Terrain.DIRT) {
			id = "D";
		}else if (terrain_ == Terrain.GRASS) {
			id = "G";
		}
		GUI_.drawCoordinateBackground(id, x_, y_);
	}
	
	public void drawObstacle() {
		String id = "-1";
		switch(obstacle_) {
		case EMPTY:
			return;
		case BOULDER:
			id = "BOULDER";
			break;
		case TREE:
			id = "TREE";
			break;
		case EXIT:
			id = "EXIT";
			break;
		}
		GUI_.drawStackCoordinate(id, x_, y_);
	}
	
	public void drawDetail() {
		String id = "-1";
		switch(detail_) {
		case EMPTY:
			return;
		case BLOOD:
			id = "BLOOD";
			break;
		}
		GUI_.drawStackCoordinate(id, x_, y_);
	}
	
	public void drawCreature() {
		if (creature_ instanceof Laura) return;
		if (creature_ != null) 
			GUI_.drawStackCoordinate(creature_.getName() + "_L", creature_.getX(), creature_.getY());
	}
	
	public void drawLaura() {
		if (creature_ instanceof Laura) {
			GUI_.drawStackCoordinate("LS", Laura.getLaura().getX(), Laura.getLaura().getY());
		}
	}
	
	public void drawLaura(Direction dir) {
		if (creature_ instanceof Laura) {
			if(dir == Direction.UP) 
				GUI_.drawStackCoordinate("LN", Laura.getLaura().getX(), Laura.getLaura().getY());
			else if(dir == Direction.DOWN)
				GUI_.drawStackCoordinate("LS", Laura.getLaura().getX(), Laura.getLaura().getY());
			else if(dir == Direction.LEFT)
				GUI_.drawStackCoordinate("LW", Laura.getLaura().getX(), Laura.getLaura().getY());
			else if(dir == Direction.RIGHT)
				GUI_.drawStackCoordinate("LE", Laura.getLaura().getX(), Laura.getLaura().getY());
		}
	}
	
	public void drawItem() {
		if (item_ == null) {
			return;
		}
		if (item_ instanceof Item) {
			GUI_.drawStackCoordinate(Integer.toString(item_.getId()) + "_L", x_, y_);
			return;
		}
		System.err.println("ERROR: Invalid item in coordinate");
	}
	
	//If creature already exists in coordinate, it cannot be overwritten
	public void setCreature(Creature cre) throws Exception {
		if(creature_ == null){
			creature_ = cre;
		}
		
		else{
			throw new Exception("Tried to place Creature over existing one");
		}
	}
	
	public boolean addCreature(Creature cre){
		if(creature_ == null){
			creature_ = cre;
			drawBackground();
			if(cre instanceof Laura){
				drawDetail();
				drawItem();
				drawLaura(Laura.getLaura().getFacingDirection());
			}
			else{
				drawAll();
			}
			return true;
		}else{
			System.out.println("ERROR: Coordinate already has a creature");
			return false;
		}
	}
	
	public boolean removeCreature(){
		if(creature_ == null){
			System.err.println("No creature was here");
			return false;
		}
		
		else{
			//Check if creature died
			if(creature_.isDead()){
				drawStats();
				setDetail(Detail.BLOOD);
			}
			creature_ = null;
			drawAll();
			return true;
		}
	}
	
	public boolean removeObstacle(){
		if(obstacle_ == Obstacle.EMPTY){
			System.err.println("No obstacle was here");
			return false;
		}
		
		else{
			obstacle_ = Obstacle.EMPTY;
			drawAll();
			return true;
		}
	}
	
	public void setInterface(GUICoordinateInterface GUI) {
		GUI_ = GUI;
	}

	public void setItem(Item item) {
		if (item != null) {
			item_ = item;
		}
		System.err.println("ERROR: Invalid item insert to coordinate.");
	}
	
	public void removeItem() {
		item_ = null;
		drawBackground();
	}

	public boolean addItem(Item item) {
		if (item_ == null && item != null) {
			if (item instanceof Item) {
				item_ = item;
				return true;
			}
		}
		System.err.println("ERROR: Invalid item add in coordinate.");
		return false;
	}
	
	public void drawStats() {
		if (creature_ != null) drawCreatureStats();
		else if (item_ != null) drawItemStats();
	}

	private void drawCreatureStats() {
		if(creature_ instanceof Laura) return;
		if(creature_ == null) return;
		GUI_.drawCreatureStats(getCreature().getName(), getCreature().getHealth(), getCreature().getMaxHealth(),
								getCreature().getStrength(), getCreature().getDefense(), getCreature().getSpeed());
	}
	
	private void drawItemStats() {
		if (item_ == null) return;
		if (item_ instanceof Potion) {
			GUI_.drawItemStats(Integer.toString(item_.getId()), 
					item_.getName(), ((Potion)item_).getHealingValue());
		}else if (item_ instanceof Weapon) {
			GUI_.drawItemStats(Integer.toString(item_.getId()), item_.getName(), 
					((Weapon)item_).getDamage(), ((Weapon)item_).getCutsWood());
		}else {
			System.err.println("ERROR: Invalid item in coordinate drawItemStats.");
		}
	}

	public boolean isCreature() {
		if(creature_ == null){
			return false;
		}
		return true;
	}
	
	public boolean isItem() {
		if (item_ != null) return true;
		return false;
	}
	
	public boolean isTree(){
		if (obstacle_ == Obstacle.TREE){
			return true;
		}
		return false;
	}
	
	public void hitCreature(int damage){
		if(creature_.reduceHealth(damage) == 0){
			removeCreature();
		}
	}
	
	public boolean isExit() {
		if (obstacle_ == Obstacle.EXIT) 
			return true;
		return false;
	}
	
	public void drawShotgunDetail(Direction dir, String position) {
		if (dir == Direction.UP)
			GUI_.drawShotgunDetail("SHOTGUN_" + position, 1, x_, y_);
		else if (dir == Direction.LEFT)
			GUI_.drawShotgunDetail("SHOTGUN_" + position, 0, x_, y_);
		else if (dir == Direction.DOWN)
			GUI_.drawShotgunDetail("SHOTGUN_" + position, 3, x_, y_);
		else if (dir == Direction.RIGHT)
			GUI_.drawShotgunDetail("SHOTGUN_" + position, 2, x_, y_);
	}
}