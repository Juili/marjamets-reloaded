package marjametsa_reloaded;

enum FactoryResult{SUCCESS, FAILURE};

public class Factory {
	protected LSFactoryInterface levelScheduler_;
	
	//Builder for LevelConfig
	public Factory(LSFactoryInterface LS) {
		levelScheduler_ = LS;
	}
	
	//Builder for Config
	public Factory() {
		
	}

	//For LevelConfig
	public FactoryResult create(Type type)  throws OutOfMemoryError {
		if (levelScheduler_ != null) {
			if (type instanceof CoordinateType) {
				Terrain terrain = null;
				switch(((CoordinateType) type).getBackground()) {
				case "D":
					terrain = Terrain.DIRT;
					break;
				case "G":
					terrain = Terrain.GRASS;
					break;
				}
				Coordinate crd = new Coordinate(((CoordinateType) type).getX(), 
						((CoordinateType) type).getY(), terrain);
				crd.setInterface(((CoordinateType) type).getGUIInterface());
				if (levelScheduler_.addCoordinate(((CoordinateType) type).getX(), 
						((CoordinateType) type).getY(), crd)) {
					return FactoryResult.SUCCESS;
				}
			}else if (type instanceof CreatureType) {
				Creature creature = null;
				
				//TODO add new creatures here
				if (((CreatureType)type).getName().equals("LYNX")) 
					creature = new Lynx();
				else if (((CreatureType)type).getName().equals("SQUIRREL")) 
					creature = new Squirrel();
				else if (((CreatureType)type).getName().equals("BEAR"))
					creature = new Bear();
				
				creature.setHealth(((CreatureType) type).getHealth());
				creature.setMaxHealth(((CreatureType) type).getMaxHealth());
				creature.setStrength(((CreatureType) type).getStrength());
				creature.setDefense(((CreatureType) type).getDefense());
				creature.setSpeed(((CreatureType) type).getSpeed());
				creature.setName(((CreatureType) type).getName());
				((Creature)creature).setX(((CreatureType) type).getX());
				((Creature)creature).setY(((CreatureType) type).getY());
				
				if (levelScheduler_.addCreature(creature)) {
					if (levelScheduler_.getCoordinate(((CreatureType) type).getX(), 
							((CreatureType) type).getY()).addCreature(creature)) {
						return FactoryResult.SUCCESS;
					}
				}
			}else if (type instanceof ItemType) {
				if (((ItemType)type).getId() < 20) { //Weapons
					Item weapon = new Weapon(((ItemType) type).getId(), ((ItemType) type).getName(), 
							((ItemType) type).getDamage(), ((ItemType) type).getCutsWood());
					if (levelScheduler_.getCoordinate(((ItemType) type).getX(), 
							((ItemType) type).getY()).addItem(weapon)) {
						return FactoryResult.SUCCESS;
					}
				}else if (((ItemType)type).getId() < 30) { //Potions
					Item weapon = new Potion(((ItemType) type).getId(), ((ItemType) type).getName(), 
							((ItemType) type).getHealingValue());
					if (levelScheduler_.getCoordinate(((ItemType) type).getX(), 
							((ItemType) type).getY()).addItem(weapon)) {
						return FactoryResult.SUCCESS;
					}
				}
			}
		}
		return FactoryResult.FAILURE;
	}
	
	public FactoryResult addCoordinateDetail() {
		return FactoryResult.SUCCESS;
	}
	
	public FactoryResult addLauratoCoordinate(int x, int y) {
		if (levelScheduler_.addLaura(Laura.getLaura())) {
			if (levelScheduler_.getCoordinate(x, y).addCreature(Laura.getLaura())) 
				return FactoryResult.SUCCESS;
		}
		return FactoryResult.FAILURE;
	}
	
	//For Config
	public FactoryResult createLauraItem(Type itemType) throws OutOfMemoryError {
		//weapon ids are 10-20, potion ids are 20-30
		if (((ItemType)itemType).getId() - 20 < 0) { //weapon
			Item weapon = new Weapon(((ItemType)itemType).getId(), ((ItemType)itemType).getName(), 
					((ItemType)itemType).getDamage(), ((ItemType)itemType).getCutsWood());
			Laura.getLaura().addItem(weapon);
			return FactoryResult.SUCCESS;
		}
		else if (((ItemType)itemType).getId() - 30 < 0) { //potion
			Item potion = new Potion(((ItemType)itemType).getId(), ((ItemType)itemType).getName(), 
					((ItemType)itemType).getHealingValue());
			Laura.getLaura().addItem(potion);
			return FactoryResult.SUCCESS;
		}
		return FactoryResult.FAILURE;
	}
	
	public void addObstacleToCoordinate(int x, int y, String id) {
		Obstacle obs = Obstacle.EMPTY;
		switch(id) {
		case "BOULDER":
			obs = Obstacle.BOULDER;
			break;
		case "TREE":
			obs = Obstacle.TREE;
			break;
		case "EXIT":
			obs = Obstacle.EXIT;
		}
		levelScheduler_.getCoordinate(x, y).setObstacle(obs);
	}
	
	public void addDetailToCoordinate(int x, int y, String id) {
		Detail detail = Detail.EMPTY;
		switch(id) {
		case "BLOOD":
			detail = Detail.BLOOD;
			break;
		}
		levelScheduler_.getCoordinate(x, y).setDetail(detail);
	}
}
