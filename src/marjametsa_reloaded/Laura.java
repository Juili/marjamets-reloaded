package marjametsa_reloaded;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("serial")
class LevelCompletedException extends Exception {

	public LevelCompletedException() {super();}
}

public class Laura extends Creature {

	private Vector<Item> items_;
	private Item itemInUse_;
	private Direction facing_;
	private GUILauraInterface UI_;
	private LSLauraInterface LS_;
	private static Laura instance = null;
	
	protected Laura(){
		facing_ = Direction.DOWN;
		items_ = new Vector<Item>();
	}
	
	//Prevents multiple instances of Laura to be built. Creates new laura (first time) and 
	//returns it or just returns already made laura.
	public static Laura getLaura() {
		if(instance == null) {
			System.out.println("Laura created");
			instance = new Laura();
		}
		return instance;
	}
	
	public int attack() {
		invariant();
		if (itemInUse_ instanceof Weapon) {
			return strength_ + ((Weapon)itemInUse_).getDamage();
		}
		return strength_;
	}
	
	public void addItem(Item item) {
		if (item == null) {
			System.err.println("Error: Tried to add null item to items_");
		}else if (items_.size() > 6) {
			System.err.println("Error: Inventory full");
		}else {
			items_.add(item);
		}
		assert items_.size() <= 6;
	}
	
	public void setX(int x) {
		assert x >= 0 && x <= LS_.getMaxX();
		super.coordinateX_ = x;
		invariant();
	}
	
	public void setY(int y) {
		assert y >= 0 && y <= LS_.getMaxY();
		super.coordinateY_ = y;
		invariant();
	}
	
	public boolean act(Direction dir) throws LevelCompletedException {
		invariant();
		Coordinate coord = LS_.coordinateAtDirection(coordinateX_, coordinateY_, dir);
		if(coord == null){
			return false;
		}
		if (coord.isItem()) {
			if (getItemCount() < 6) {
				addItem(coord.getItem());
				coord.removeItem();
				drawItems();
			}
		}
		
		//Hit creature in direction
		if(coord.isCreature()){
			facing_ = dir;
			LS_.getCoordinate(coordinateX_, coordinateY_).drawAll();
			coord.hitCreature(attack());
			coord.drawStats();			
			//decrease moves
			movesLeft_ = 0;
			drawStats();
		}
		
		else if(coord.isObstacle()){
			facing_ = dir;
			if(coord.isTree() && (itemInUse_ != null) && itemInUse_.getName().equals("Axe")){ //TODO use better method
				LS_.getCoordinate(coordinateX_, coordinateY_).drawAll();
				coord.removeObstacle();
				--movesLeft_;
				System.out.println("Tree cut");
			}else if (coord.isExit()) { //Exit level tile
				if (LS_.countCreatures() == 1) { //Laura is the only creature
					System.out.println("Level completed");
					itemInUse_ = null;
					throw new LevelCompletedException();
				}else {
					System.out.println("Cannot exit, because there are creatures still left");
				}
			}else{
				LS_.getCoordinate(coordinateX_, coordinateY_).drawAll();
				System.out.println("Obstacle blocking me!");
			}
		}
		
		else{
			//remove Laura from old coordinate
			if(!LS_.removeCreature(coordinateX_, coordinateY_)){
				invariant();
				return false;
			}
			
			//move to new direction
			move(dir);
			facing_ = dir;
			
			//add creature to new coordinate
			LS_.addCreature(coordinateX_, coordinateY_, this);
			
			//decrease moves
			--movesLeft_;
			drawStats();
			
		}
		invariant();
		return true;
	}
	
	public int getItemCount() {
		assert items_.size() <= 6;
		return items_.size();
	}
	
	public void clearItems() {
		items_.clear();
	}
	
	public Item getItem(int index) throws ArrayIndexOutOfBoundsException {
		assert items_.size() <= 6;
		return items_.get(index);
	}
	
	public Direction getFacingDirection(){
		invariant();
		return facing_;
	}
	
	public int deleteItem(int index) throws ArrayIndexOutOfBoundsException {
		invariant();
		if (getItem(index) == itemInUse_) {
			itemInUse_ = null;
		}
		items_.remove(index);
		invariant();
		return items_.size();
	}
	
	public void setUI(GUILauraInterface UI) {
		UI_ = UI;
		invariant();
	}
	
	public void setItemInUse(Item item, int itemIndex) {
		invariant();
		if (item instanceof Potion) {
			health_ += ((Potion)item).getHealingValue();
			if (health_ > maxHealth_) health_ = maxHealth_;
			drawStats();
			try {
				deleteItem(itemIndex);
			}catch(ArrayIndexOutOfBoundsException e) {
				System.err.println("ERROR: Invalid item index for potion used.");
			}
			invariant();
			return;
		}
		itemInUse_ = item;
		if (itemInUse_ instanceof Weapon) 
			drawStats();
		UI_.drawItemInUse(itemInUse_.getId());
		invariant();
	}
	
	public void drawItemStats(Item item) {
		invariant();
		if (item instanceof Potion) {
			UI_.drawItemStats(Integer.toString(item.getId()), 
					item.getName(), ((Potion)item).getHealingValue());
		}else if (item instanceof Weapon) {
			UI_.drawItemStats(Integer.toString(item.getId()), item.getName(), 
					((Weapon)item).getDamage(), ((Weapon)item).getCutsWood());
		}else {
			System.err.println("ERROR: Invalid item in drawItemStats.");
		}
		invariant();
	}
	
	public Item getItemInUse() {
		invariant();
		return itemInUse_;
	}
	
	public void removeItemInUse() {
		invariant();
		itemInUse_ = null;
		UI_.drawItemInUse(-1); //empty value
		drawStats();
		invariant();
	}
	
	public void drawItems() {
		invariant();
		//sends id array to UI
		Vector<Integer> ids = new Vector<Integer>();
		for (int i = 0; i < items_.size(); ++i) {
			ids.add(items_.get(i).getId());
		}
		UI_.drawItems(ids);
		invariant();
	}

	public void drawStats() {
		invariant();
		UI_.drawLauraStats(health_, maxHealth_, attack(), defense_, movesLeft_);
	}
	
	public void turnLaura(Direction dir) {
		invariant();
		facing_ = dir;
		LS_.getCoordinate(coordinateX_, coordinateY_).drawAll();
		invariant();
	}
	
	public void setLSInterface(LSLauraInterface lsInt) {
		LS_ = lsInt;
		assert LS_ != null;
	}
	
	public boolean shoot(Direction dir) {
		invariant();
		try {
			if (!getItemInUse().getName().equals("Shotgun")) {
				System.out.println("Cannot shoot with " + getItemInUse().getName());
				return false;
			}
		} catch(NullPointerException e) {
			System.out.println("Cannot shoot with no item in use");
			return false;
		}
		damageByShooting(dir);
		drawShotgunSpread(dir);
		invariant();
		return true;
	}
	
	private void damageByShooting(Direction dir) {
		invariant();
		//order from laura's perspective: near, far left, far center, far right
		Vector<Coordinate> spreadCoordinates = getSpreadCoordinates(dir); 
		if (spreadCoordinates.size() != 4) {
			System.err.println("ERROR: Logic error in spread coordinates.");
		}
		if (spreadCoordinates.get(0) == null) return;
		if (spreadCoordinates.get(0).isObstacle()) return; //cannot shoot while obstacle is blocking
		for (int i = 0; i < spreadCoordinates.size(); ++ i) {
			if (spreadCoordinates.get(i) == null) continue;
			if (i == 0) { //Deals more damage up close
				if (spreadCoordinates.get(i).isCreature()) {
					spreadCoordinates.get(i).hitCreature(attack());
					continue;
				}
			}
			if (spreadCoordinates.get(i).isCreature()) {
				spreadCoordinates.get(i).hitCreature(attack() / 2); //half the damage to far away creatures
				spreadCoordinates.get(i).drawStats();
			}
		}
		if (spreadCoordinates.get(0).isCreature()) spreadCoordinates.get(0).drawStats(); //prioritizes closest enemy stats
		movesLeft_ = 0;
		drawStats();
		invariant();
	}
	
	private void drawShotgunSpread(Direction dir) { //TODO
		invariant();
		turnLaura(dir);
		
		//order from laura's perspective: near, far left, far center, far right
		Vector<Coordinate> spreadCoordinates = getSpreadCoordinates(dir); 
		if (spreadCoordinates.size() != 4) {
			System.err.println("ERROR: Logic error in spread coordinates.");
		}
		for (int i = 0; i < spreadCoordinates.size(); ++ i) {
			if (spreadCoordinates.get(i) == null) continue;
			if (i == 0) {
				if (spreadCoordinates.get(i).isObstacle()) return; //cannot shoot through obstacle
				spreadCoordinates.get(i).drawShotgunDetail(dir, "NEAR");
			}
			else if (i == 1) {
				if (spreadCoordinates.get(i).isObstacle()) continue; //obstacle far doesn't make shooting impossible
				spreadCoordinates.get(i).drawShotgunDetail(dir, "FAR_LEFT");
			}
			else if (i == 2) {
				if (spreadCoordinates.get(i).isObstacle()) continue;
				spreadCoordinates.get(i).drawShotgunDetail(dir, "FAR_CENTER");
			}
			else if (i == 3) {
				if (spreadCoordinates.get(i).isObstacle()) continue;
				spreadCoordinates.get(i).drawShotgunDetail(dir, "FAR_RIGHT");
			}
		}
		
		UI_.undrawShotgunDetail(spreadCoordinates);
		
		//threadSleep(1000); //milliseconds
		
		/*for (int i = 0; i < spreadCoordinates.size(); ++ i) { //removes shotgun detail
			if (spreadCoordinates.get(i) == null) continue;
			spreadCoordinates.get(i).drawShotgunDetail(dir, "FAR_CENTER");
			System.out.println("poisto");
		}*/
		
		invariant();
	}
	
	public void threadSleep(int msDuration) {
		try {
			TimeUnit.MILLISECONDS.sleep(msDuration);
		}catch (InterruptedException e) {
		    System.err.println("ERROR: Sleep interrupted.");
		}
	}
	
	@SuppressWarnings("incomplete-switch")
	private Vector<Coordinate> getSpreadCoordinates(Direction dir) {
		invariant();
		Vector<Coordinate> spreadC = new Vector<Coordinate>();
		switch(dir) {
		case UP:
			spreadC.add(LS_.getCoordinate(coordinateX_, coordinateY_ - 1)); //near
			spreadC.add(LS_.getCoordinate(coordinateX_ - 1, coordinateY_ - 2)); //far left
			spreadC.add(LS_.getCoordinate(coordinateX_, coordinateY_ - 2)); //far center
			spreadC.add(LS_.getCoordinate(coordinateX_ + 1, coordinateY_ - 2)); //far right
			break;
		case RIGHT:
			spreadC.add(LS_.getCoordinate(coordinateX_ + 1, coordinateY_)); //near
			spreadC.add(LS_.getCoordinate(coordinateX_ + 2, coordinateY_ - 1)); //far up
			spreadC.add(LS_.getCoordinate(coordinateX_ + 2, coordinateY_)); //far center
			spreadC.add(LS_.getCoordinate(coordinateX_ + 2, coordinateY_ + 1)); //far down
			break;
		case DOWN:
			spreadC.add(LS_.getCoordinate(coordinateX_, coordinateY_ + 1)); //near
			spreadC.add(LS_.getCoordinate(coordinateX_ + 1, coordinateY_ + 2)); //far right
			spreadC.add(LS_.getCoordinate(coordinateX_, coordinateY_ + 2)); //far center
			spreadC.add(LS_.getCoordinate(coordinateX_ - 1, coordinateY_ + 2)); //far left
			break;
		case LEFT:
			spreadC.add(LS_.getCoordinate(coordinateX_ - 1, coordinateY_)); //near
			spreadC.add(LS_.getCoordinate(coordinateX_ - 2, coordinateY_ + 1)); //far down
			spreadC.add(LS_.getCoordinate(coordinateX_ - 2, coordinateY_)); //far center
			spreadC.add(LS_.getCoordinate(coordinateX_ - 2, coordinateY_ - 1)); //far up
			break;
		}
		invariant();
		return spreadC;
	}
	
	private void invariant() {
		assert items_.size() <= 6;
		assert UI_ != null;
		assert LS_ != null;
		assert health_ <= maxHealth_;
		assert coordinateX_ >= 0 && coordinateX_ <= LS_.getMaxX();
		assert coordinateY_ >= 0 && coordinateY_ <= LS_.getMaxY();
		assert movesLeft_ >= 0 && movesLeft_ <= speed_;
		assert name_.equals("Laura");
	}
 }
