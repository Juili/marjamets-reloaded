package marjametsa_reloaded;

import java.awt.BorderLayout;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JTextField;

public class LevelUI extends JFrame implements LvlMenuGEInterface {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textfield_;
	private MouseListener GEMouse_;
	private ActionListener GEButton_;

	public LevelUI(GameEngineInterface GE) {
		GEMouse_ = (MouseListener)GE;
		GEButton_ = (ActionListener)GE;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1024, 768);
		setLocationRelativeTo(null);
		BufferedImage image = null;
		try{
			System.out.println(ImageLocations.getImageLocations().getFilePath("LEVEL"));
			image = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("LEVEL")));
		}catch (Exception ex){
			System.err.println("ERROR: Could not load background");
		}
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel levels = new JPanelWithBackground(image);
		contentPane.add(levels, BorderLayout.CENTER);
		
		BufferedImage image1 = null;
		try{
			image1 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("1")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level1 = new JPanelLevel(image1, 1);
		level1.setLocation(10, 60);
		level1.addMouseListener(GEMouse_);
		levels.setLayout(null);
		levels.add(level1);
		level1.setSize(50,50);
		
		BufferedImage image2 = null;
		try{
			image2 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("2")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level2 = new JPanelLevel(image2, 2);
		level2.setLocation(150, 200);
		level2.setSize(50,50);
		level2.addMouseListener(GEMouse_);
		levels.add(level2);
		
		BufferedImage image3 = null;
		try{
			image3 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("3")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level3 = new JPanelLevel(image3, 3);
		level3.setLocation(290, 290);
		level3.setSize(50,50);
		level3.addMouseListener(GEMouse_);
		levels.add(level3);
		
		BufferedImage image4 = null;
		try{
			image4 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("4")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level4 = new JPanelLevel(image4, 4);
		level4.setSize(50,50);
		level4.setLocation(450, 120);
		level4.addMouseListener(GEMouse_);
		levels.add(level4);
		
		BufferedImage image5 = null;
		try{
			image5 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("5")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level5 = new JPanelLevel(image5, 5);
		level5.setSize(50,50);
		level5.setLocation(550, 300);
		level5.addMouseListener(GEMouse_);
		levels.add(level5);
		
		BufferedImage image6 = null;
		try{
			image6 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("6")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level6 = new JPanelLevel(image6, 6);
		level6.setSize(50,50);
		level6.setLocation(600, 500);
		level6.addMouseListener(GEMouse_);
		levels.add(level6);

		BufferedImage image7 = null;
		try{
			image7 = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("7")));
		}catch (Exception e){
			System.err.println("ERROR: Could not load level numbers");
		}
		JPanel level7 = new JPanelLevel(image7, 7);
		level7.setSize(50,50);
		level7.setLocation(800, 600);
		level7.addMouseListener(GEMouse_);
		levels.add(level7);
		
		JPanel southpanel = new JPanel();
		contentPane.add(southpanel, BorderLayout.SOUTH);
		
		JButton save = new JButtonLevel("Save");
		save.setText("Save");
		save.addActionListener(GEButton_);
		southpanel.add(save);
		
		textfield_ = new JTextField(null);
		textfield_.setColumns(12);
		textfield_.setOpaque(true);
		textfield_.setEditable(false);
		textfield_.setBorder(null);
		southpanel.add(textfield_);
		
		textfield_.setVisible(true);
		
		this.setVisible(true);
	}
	
	public void printGameSaved(boolean success){
		if (success){
			textfield_.setText("Game progress saved");
			textfield_.setVisible(true);
		}else{
			textfield_.setText("Saving failed");
			textfield_.setVisible(true);
		}
	}
	
	public void notifyLevelLocked(){
		textfield_.setText("Level is Locked");
		textfield_.setVisible(true);
	}
	
	public void hideText(){
		textfield_.setVisible(false);
	}
}
