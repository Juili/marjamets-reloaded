package marjametsa_reloaded;

public class ItemType extends Type {
	
	private int id_;
	private String name_;
	private int damage_;
	private boolean cutsWood_;
	private int healingValue_;
	private int x_;
	private int y_;
	
	public ItemType() {
		
	}

	public void setId(int id) {
		id_ = id;
	}
	
	public void setName(String name) {
		name_ = name;
	}
	
	public void setDamage(int damage) {
		damage_ = damage;
	}
	
	public void setCutsWood(boolean cuts) {
		cutsWood_ = cuts;
	}
	
	public void setHealingValue(int healing) {
		healingValue_ = healing;
	}
	
	public void setX(int x) {
		x_ = x;
	}

	public void setY(int y) {
		y_ = y;
	}
	
	public int getId() {
		return id_;
	}
	
	public String getName() {
		return name_;
	}
	
	public int getDamage() {
		return damage_;
	}
	
	public boolean getCutsWood() {
		return cutsWood_;
	}
	
	public int getHealingValue() {
		return healingValue_;
	}

	public int getX() {
		return x_;
	}

	public int getY() {
		return y_;
	}
}
