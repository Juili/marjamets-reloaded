package marjametsa_reloaded;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	
	private static final int button_location_x_ = 300;  	//location x 
	private static final int button_location_y_ = 300;   	//location y 
	private static final int button_size_x_ = 200;      	//size width
	private static final int button_size_y_ = 50;       	//size height
	
	private static final int main_window_size_x_ = 800;		//size width
	private static final int main_window_size_y_ = 600;   	//size height
	
	private String upperCornerIcon_ = "danger-icon.jpg";
	private String backgroundImage_ = "marjametsa_image.jpg";
	
	private String newGameButtonImageName_ = "newgamebutton.gif";
	private String loadGameButtonImageName_ = "loadgamebutton.gif";
	private String optionsButtonImageName_ = "optionsbutton.gif";
	
	private static JFrame menuFrame_;
	private static JFrame gamingFrame_;
	private static Container pane_;
	
	private KeyListener GEKey_;
	
	private static GraphicsDevice device_ = GraphicsEnvironment
	        .getLocalGraphicsEnvironment().getScreenDevices()[0];
	
	public GUI(GameEngineInterface gei) {
		GEKey_ = (KeyListener)gei;
	}
	
	public void createMenuUI() {
		//creating menu frame
		menuFrame_ = new JFrame("Marjametsä Reloaded");
		menuFrame_.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menuFrame_.setSize(main_window_size_x_, main_window_size_y_);
		menuFrame_.setLocationRelativeTo(null);
		menuFrame_.setLayout(null);
		menuFrame_.setIconImage(new ImageIcon("pictures\\" + upperCornerIcon_).getImage());
		

		//menu frame background image
		try {
			BufferedImage image = ImageIO.read(new File("pictures\\" + backgroundImage_));
			menuFrame_.setContentPane(new ImagePanel(image));
		} catch (IOException e) {
			System.err.println("Error loading backgroung image");
			e.printStackTrace();
		}
		
		//button images
		final JButton newGameButton = new JButton();
		final JButton loadGameButton = new JButton();
		final JButton optionsButton = new JButton();
		addButtonImage(newGameButton, newGameButtonImageName_);
		addButtonImage(loadGameButton, loadGameButtonImageName_);
		addButtonImage(optionsButton, optionsButtonImageName_);

		//newGameButton.setBounds(300, 300, 200, 50 );
		newGameButton.setBounds(button_location_x_, 
								button_location_y_,
								button_size_x_,
								button_size_y_);
		loadGameButton.setBounds(button_location_x_, 
								button_location_y_ + 70,
								button_size_x_, 
								button_size_y_);
		optionsButton.setBounds(button_location_x_, 
								button_location_y_ + 140,
								button_size_x_, 
								button_size_y_);
		
		//Button action listeners		
        newGameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent n)
            {
            	//Changes button image and opens new window
            	newGameButtonImageName_ = "newgamebuttonclicked.gif";
        		addButtonImage(newGameButton, newGameButtonImageName_);
            	createFrame("New Game", menuFrame_, n);
            }
        });  
		
        loadGameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent l)
            {
            	loadGameButtonImageName_ = "loadgamebuttonclicked.gif";
            	addButtonImage(loadGameButton, loadGameButtonImageName_);
            	createFrame("Load Game", menuFrame_, l);
            }
        });
        
        optionsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent o)
            {
            	optionsButtonImageName_ = "optionsbuttonclicked.gif";
            	addButtonImage(optionsButton, optionsButtonImageName_);
            	createFrame("Options", menuFrame_, o);
            }
        });
        
        JPanel listenerPanel = new JPanel();
		listenerPanel.addKeyListener(GEKey_);
		menuFrame_.add(listenerPanel);
		
		
        
		//container for buttons
		Container buttonsPane = menuFrame_.getContentPane();
		buttonsPane.add(newGameButton);
		buttonsPane.add(loadGameButton);
		buttonsPane.add(optionsButton);
	
		menuFrame_.setVisible(true);
		listenerPanel.requestFocusInWindow();
	}
	
	public void showMenuUI(boolean trueOrFalse) {
		menuFrame_.setVisible(trueOrFalse);
	}
	
	public void createGamingUI() {
		System.out.println("GamingUI");
		gamingFrame_ = new JFrame("Marjametsä Reloaded");
	    gamingFrame_.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	    pane_ = gamingFrame_.getContentPane();
	    
	    JButton button = new JButton("Button 1 (PAGE_START)");
	    JButton button2 = new JButton("Button 2 (LINE_START)");
	    JButton button3 = new JButton("Button 3 (CENTER)");
	    
	    pane_.add(button2, BorderLayout.LINE_START);
        pane_.add(button, BorderLayout.PAGE_START);
        pane_.add(button3, BorderLayout.CENTER);
        device_.setFullScreenWindow(gamingFrame_);
	}
	
	public void showGamingUI(boolean trueOrFalse) {
		gamingFrame_.setVisible(trueOrFalse);
	}
	
	public void createLevelSelectUI() {
		System.out.println("createLevelSelectUI");
	}
	
	public void showLevelSelectUI(boolean trueOrFalse) {
		System.out.println("showLevelSelectUI");
	}
	
	public void createOptionsUI() {
		System.out.println("createOptionsUI");
		
	}
	
	public void showOptionsUI(boolean trueOrFalse) {
		System.out.println("showOptionsUI");
	}
	
	//Create new full sized gaming frame
	private void createFrame(String name, JFrame menu, ActionEvent e) {
        System.out.println(name + " clicked");
    	Object source = e.getSource();
        if (source instanceof JButton) {
            //JButton btn = (JButton)source;
            menu.setVisible(false);
        }
	}
	
	//add button images
	private void addButtonImage(JButton button, String image) {
		try {
			Image buttonImage = ImageIO.read(new File("pictures\\" + image));
			button.setIcon(new ImageIcon(buttonImage));
		} catch (IOException ex) {
			System.err.println("Error loading image " + image);
			ex.printStackTrace();
		}
	}

}

//Class to make background image
class ImagePanel extends JComponent {

	private static final long serialVersionUID = 1L;
	private Image image;
	public ImagePanel(Image backgroundimage) {
		this.image = backgroundimage;
	}
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, null);
	}
}
