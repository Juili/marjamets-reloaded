package marjametsa_reloaded;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Vector;

enum LevelConfiguration{SUCCESS, FAILURE}

public class LevelConfig {
	
	private LSFactoryInterface levelScheduler_;
	private GUICoordinateInterface coordinateGUI_;
	private GUILauraInterface LauraGUI_;
	private Factory factory_;
	private Path path_;
	private final static Charset ENCODING = StandardCharsets.UTF_8;
	private boolean creatingObjectFailed_;
	private Vector<Vector<String>> coordBackground_;
	private int lastRow_;
	private List<String> lines_;
	
	public LevelConfig(LSFactoryInterface LS, int levelNumber, 
									GUICoordinateInterface coordinateGUI, 
									GUILauraInterface GUILaura) {
		levelScheduler_ = LS;
		coordinateGUI_ = coordinateGUI;
		LauraGUI_ = GUILaura;
		factory_ = new Factory(levelScheduler_);
		path_ = FileSystems.getDefault().getPath("configFiles", 
				"level" + Integer.toString(levelNumber) + ".txt");
		creatingObjectFailed_ = false;
		coordBackground_ = new Vector<Vector<String>>();
		lastRow_ = 0;
		lines_ = null;
	}
	
	public LevelConfiguration setLevel() {
		try {
			lines_ = readFile();
			if (readLevelBackground()) {
				if (!setCoordinates()) return LevelConfiguration.FAILURE;
				if (!setLauraCoordinate()) return LevelConfiguration.FAILURE;
				while (setCreature());
				setDetails();
				setObstacles();
				while (setItem());
				if (creatingObjectFailed_) return LevelConfiguration.FAILURE;
				
				return LevelConfiguration.SUCCESS;
			}
		}catch(IOException e) {
			System.err.println("ERROR: " + path_ + " not found.");
		}catch(OutOfMemoryError ex) {
			System.err.println("ERROR: Out of memory.");
			System.exit(1);
		}
		return LevelConfiguration.FAILURE;
	}
	
	@SuppressWarnings("unchecked")
	private boolean readLevelBackground() throws IOException {
		Vector<String> row = new Vector<String>();
		String line = "";
		String sublines[];
		int rowNum = 0;
		while (lastRow_ < lines_.size()) {
			sublines = null;
			line = lines_.get(lastRow_);
			++lastRow_;
			sublines = line.split(":");
			if (sublines[0].equals("M")) {
				for (int j = 0; j < sublines[1].length(); ++j) {
					row.add(sublines[1].substring(j, j+1));
				}
				if (rowNum > 0) {
					//Checks that every tile row is equally long
					if (row.size() != coordBackground_.get(rowNum - 1).size()) {
						System.err.println("ERROR: Level tile row invalid.");
						return false;
					}
				}
				coordBackground_.add(rowNum, row);
				coordBackground_.set(rowNum,(Vector<String>)row.clone());
				++rowNum;
				row.clear();
			}
		}
		lastRow_ = 0;
		if (coordBackground_.isEmpty()) {
			System.err.println("ERROR: No level tiles read.");
			return false; //checks if any rows were inserted
		}
		levelScheduler_.setLevelMaxSize(coordBackground_.get(0).size() - 1, 
				coordBackground_.size() - 1);
		return true;
	}
	
	//Reads file to memory
	private List<String> readFile() throws IOException {
		System.out.println("Reads " + path_.toUri());
		return Files.readAllLines(path_, ENCODING);
	}
	
	//Splits line by comma
	private String[] separateByComma(String attributes) {
		String split[] = null;
		split = attributes.split(",");
		return split;
	}
	
	private Type createCoordinateType(int inner, int outer) throws OutOfMemoryError {
		Type type = new CoordinateType(coordinateGUI_);
		((CoordinateType)type).setX(inner);
		((CoordinateType)type).setY(outer);
		((CoordinateType)type).setBackground(coordBackground_.get(outer).get(inner));
		return type;
	}
	
	private Type createCreatureType(String line) throws OutOfMemoryError {
		Type type = new CreatureType();
		String attributes[] = separateByComma(line);
		((CreatureType)type).setX(Integer.parseInt(attributes[0]));
		((CreatureType)type).setY(Integer.parseInt(attributes[1]));
		((CreatureType)type).setName(attributes[2]);
		((CreatureType)type).setHealth(Integer.parseInt(attributes[3]));
		((CreatureType)type).setMaxHealth(Integer.parseInt(attributes[4]));
		((CreatureType)type).setSpeed(Integer.parseInt(attributes[5]));
		((CreatureType)type).setStrength(Integer.parseInt(attributes[6]));
		((CreatureType)type).setDefense(Integer.parseInt(attributes[7]));
		return type;
	}
	
	private Type createItemType(String iType, String line) throws OutOfMemoryError {
		Type type = new ItemType();
		String attributes[] = separateByComma(line);
		((ItemType)type).setX(Integer.parseInt(attributes[0]));
		((ItemType)type).setY(Integer.parseInt(attributes[1]));
		((ItemType)type).setId(Integer.parseInt(attributes[2]));
		((ItemType)type).setName(attributes[3]);
		if (iType.equals("P")) {
			((ItemType)type).setHealingValue(Integer.parseInt(attributes[4]));
		}else if (iType.equals("W")) {
			((ItemType)type).setDamage(Integer.parseInt(attributes[4]));
			if (attributes[5].equals("1")) { //cutsWood boolean
				((ItemType)type).setCutsWood(true);
			}else {
				((ItemType)type).setCutsWood(false);
			}
		}
		return type;
	}
	
	private boolean setCoordinates() {
		int outer;
		int inner = 0;
		for (outer = 0; outer < coordBackground_.size(); ++outer) {
			for (inner = 0; inner < coordBackground_.get(outer).size(); ++inner) {
				if (factory_.create(createCoordinateType(inner, outer)) == FactoryResult.FAILURE) {
					creatingObjectFailed_ = true;
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean setCreature() throws OutOfMemoryError {
		String sublines[] = null;
		while (lastRow_ < lines_.size()) {
			sublines = lines_.get(lastRow_).split(":");
			++lastRow_;
			if (sublines[0].equals("C")) {
				if (factory_.create(createCreatureType(sublines[1])) == FactoryResult.FAILURE) {
					creatingObjectFailed_ = true;
					return false;
				}
				return true;
			}
		}
		lastRow_ = 0;
		return false;
	}
	
	private boolean setLauraCoordinate() throws OutOfMemoryError {
		Laura.getLaura().setUI(LauraGUI_);
		String sublines[] = null;
		for (int rowNum = 0; rowNum < lines_.size(); ++rowNum) {
			sublines = lines_.get(rowNum).split(":");
			if (sublines[0].equals("L")) {
				String coordinates[] = separateByComma(sublines[1]);
				Laura.getLaura().setX(Integer.parseInt(coordinates[0]));
				Laura.getLaura().setY(Integer.parseInt(coordinates[1]));
				factory_.addLauratoCoordinate(Integer.parseInt(coordinates[0]), 
						Integer.parseInt(coordinates[1]));
				return true;
			}
		}
		System.err.println("ERROR: No coordinates for Laura");
		return false;
	}
	
	private boolean setItem() throws OutOfMemoryError {
		String sublines[] = null;
		while (lastRow_ < lines_.size()) {
			sublines = lines_.get(lastRow_).split(":");
			++lastRow_;
			if (sublines[0].equals("P")) {
				if (factory_.create(createItemType(sublines[0], sublines[1])) 
						== FactoryResult.FAILURE) {
					creatingObjectFailed_ = true;
					return false;
				}
			}else if(sublines[0].equals("W")) {
				if (factory_.create(createItemType(sublines[0], sublines[1])) 
						== FactoryResult.FAILURE) {
					creatingObjectFailed_ = true;
					return false;
				}
			}
			return true;
		}
		lastRow_ = 0;
		return false;
	}
	
	private void setObstacles() throws OutOfMemoryError {
		String sublines[] = null;
		for (int rowNum = 0; rowNum < lines_.size(); ++rowNum) {
			sublines = lines_.get(rowNum).split(":");
			if (sublines[0].equals("O")) {
				String attributes[] = separateByComma(sublines[1]);
				factory_.addObstacleToCoordinate(Integer.parseInt(attributes[0]), 
						Integer.parseInt(attributes[1]), attributes[2]);
			}
		}
	}
	
	private void setDetails() throws OutOfMemoryError {
		String sublines[] = null;
		for (int rowNum = 0; rowNum < lines_.size(); ++rowNum) {
			sublines = lines_.get(rowNum).split(":");
			if (sublines[0].equals("D")) {
				String attributes[] = separateByComma(sublines[1]);
				factory_.addDetailToCoordinate(Integer.parseInt(attributes[0]), 
						Integer.parseInt(attributes[1]), attributes[2]);
			}
		}
	}
}
