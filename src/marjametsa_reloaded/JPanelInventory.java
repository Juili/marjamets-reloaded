package marjametsa_reloaded;

import java.awt.Color;
import java.awt.Image;

import javax.swing.BorderFactory;

public class JPanelInventory extends JPanelWithBackground {

	private static final long serialVersionUID = 1L;
	private int orderFromTop_;
	
	public JPanelInventory(Image i, int orderNum) {
		super(i);
		if (orderNum < 1 || orderNum > 6) {
			System.err.println("Error: Invalid order number for inventory");
		}else {
			orderFromTop_ = orderNum;
		}
	}
	public int getOrderFromTop() {
		return orderFromTop_;
	}
	
	public void drawBorder(boolean isSelected) {
		if (isSelected) {
			setBorder(BorderFactory.createLineBorder(Color.black));
		}else {
			setBorder(null);
		}
		setOpaque(false);
		repaint();
	}
}
