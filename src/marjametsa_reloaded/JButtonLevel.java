package marjametsa_reloaded;

import javax.swing.JButton;

public class JButtonLevel extends JButton {

	private static final long serialVersionUID = 1L;
	private String name_;
	
	public JButtonLevel(String name) {
		super();
		name_ = name;
	}
	
	public String getName() {
		return name_;
	}
}
