package marjametsa_reloaded;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Bear extends Creature {
	
	private int range_;
	
	public Bear(){
		range_ = 4;
	}
	
	@Override
	public boolean act(LevelScheduler LS) {
		while (movesLeft_ > 0) {	
			// Check if Creature is RIGHT to you
			try{
				if(LS.getCoordinate(coordinateX_+ 1, coordinateY_).isCreature()){
					LS.getCoordinate(coordinateX_+ 1, coordinateY_).hitCreature(strength_);
					movesLeft_ = 0;
					return true;
				}
			}catch(NullPointerException n){
				
			}
			
			// Check if Creature is LEFT to you
			try{
				if(LS.getCoordinate(coordinateX_- 1, coordinateY_).isCreature()){
					LS.getCoordinate(coordinateX_- 1, coordinateY_).hitCreature(strength_);
					movesLeft_ = 0;
					return true;
				}
			}catch(NullPointerException n){
				
			}
			
			// Check if Creature is NORTH to you
			try{
				if(LS.getCoordinate(coordinateX_, coordinateY_- 1).isCreature()){
					LS.getCoordinate(coordinateX_, coordinateY_- 1).hitCreature(strength_);
					movesLeft_ = 0;
					return true;
				}
			}catch(NullPointerException n){
				
			}
			
			// Check if Creature is SOUTH to you
			try{
				if(LS.getCoordinate(coordinateX_, coordinateY_+ 1).isCreature()){
					LS.getCoordinate(coordinateX_, coordinateY_+ 1).hitCreature(strength_);
					movesLeft_ = 0;
					return true;
				}
			}catch(NullPointerException n){
				
			}
			
			//Check closest creature in range
			Creature closest = closestInRange(LS, range_);
			
			if(closest != null){
				// -----MOVE TOWARDS CLOSEST-----
				// check directions
				ArrayList<Direction> moveDirections = new ArrayList<Direction>();

				if (closest.getX() > coordinateX_) {
					moveDirections.add(Direction.RIGHT);
				}

				if (closest.getX() < coordinateX_) {
					moveDirections.add(Direction.LEFT);
				}

				if (closest.getY() < coordinateY_) {
					moveDirections.add(Direction.UP);
				}

				if (closest.getY() > coordinateY_) {
					moveDirections.add(Direction.DOWN);
				}

				long seed = System.nanoTime();
				Collections.shuffle(moveDirections, new Random(seed));

				// check possible direction to move
				Direction moveDecision = Direction.NOWHERE;

				while (moveDirections.size() > 0) {
					if (moveDirections.get(0) == Direction.UP) {
						if (LS.ableToMove(coordinateX_, coordinateY_ - 1)) {
							moveDecision = Direction.UP;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.DOWN) {
						if (LS.ableToMove(coordinateX_, coordinateY_ + 1)) {
							moveDecision = Direction.DOWN;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.LEFT) {
						if (LS.ableToMove(coordinateX_ - 1, coordinateY_)) {
							moveDecision = Direction.LEFT;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.RIGHT) {
						if (LS.ableToMove(coordinateX_ + 1, coordinateY_)) {
							moveDecision = Direction.RIGHT;
							break;
						}
					}

					moveDirections.remove(0);
				}

				// If no possible directions to move, creature stays where it
				// is,
				// and function returns true
				if (moveDirections.size() == 0) {
					--movesLeft_;
					return true;
				}

				// remove creature from old coordinate
				if (!LS.removeCreature(coordinateX_, coordinateY_)) {
					return false;
				}

				// move to new direction
				this.move(moveDecision);

				// add creature to new coordinate
				LS.addCreature(coordinateX_, coordinateY_, this);

				// Reduce movesleft
				--movesLeft_;
			}
			
			else{
				//move to random direction //TODO
				
				--movesLeft_;
			}
		}
		return true;
	}
	
}
