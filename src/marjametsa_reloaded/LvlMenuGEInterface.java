package marjametsa_reloaded;

public interface LvlMenuGEInterface {
	public void setVisible(boolean visible);
	public void printGameSaved(boolean success);
	public void notifyLevelLocked();
	public void hideText();
}
