package marjametsa_reloaded;

public interface LSFactoryInterface {
	
	public boolean addCoordinate(int x, int y, Coordinate coordinate);
	public boolean addCreature(Creature creature);
	public Coordinate getCoordinate(int x, int y);
	public boolean addLaura(Laura L);
	public void setLevelMaxSize(int maxX, int maxY);
}
