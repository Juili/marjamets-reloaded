package marjametsa_reloaded;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;


public class GameEngine extends MouseAdapter implements KeyListener, ActionListener, 
														GameEngineInterface, GEConfigInterface {
	
	private static int openLevels_;
	private static LevelScheduler scheduler_;
	private JPanelInventory lastInventoryClicked_;
	private static GUIGEInterface gamingUI_;
	private static LvlMenuGEInterface lvlUI_;
	private static SMenuGEInterface menuUI_;
	private boolean playingOldLevel_ = false;
	
	public GameEngine() {
		
	}
	
	public void setOpenLevels(int levels) {
		assert levels >= 1 && levels <= 7;
		openLevels_ = levels;
		assert openLevels_ == levels;
	}
	
	public static void main(String[] args) {
		GameEngine GE = new GameEngine();
		try {
			menuUI_ = new StartMenu((GameEngineInterface)GE);
			scheduler_ = new LevelScheduler();
		}catch (OutOfMemoryError e) {
			System.err.println("Error: Out of Memory");
			System.exit(1);
		}
		assert menuUI_ != null && scheduler_ != null;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		invariant();
		Object source = e.getSource();
		if (source instanceof JPanelInventory) { //inventory
			inventoryClick(source);
		}else if (source instanceof JPanelPoint) { //Level tile
			levelTileClick(source);
		}else if (source instanceof JPanelLevel){ //Level selection
			levelSelectionClick(source);
		}else {
			//Do nothing
		}
		invariant();
	}
	
	private void inventoryClick(Object source) {
		System.out.println("Inventory " + ((JPanelInventory)source).getOrderFromTop() 
				+ " clicked");
		if (lastInventoryClicked_ != null) {
			//erase previous border
			gamingUI_.drawBorder(lastInventoryClicked_.getOrderFromTop(), false);
		}
		try {
			Laura.getLaura().drawItemStats(Laura.getLaura().getItem(
				((JPanelInventory) source).getOrderFromTop() - 1)); //draws stats in upper panel
			lastInventoryClicked_ = (JPanelInventory)source;
			gamingUI_.drawBorder(lastInventoryClicked_.getOrderFromTop(), true); //draws border on clicked inventory
		}catch(ArrayIndexOutOfBoundsException ex) {
			System.out.println("No item stats to be drawn");
		}
	}
	
	private void levelTileClick(Object source) {
		System.out.println("Map tile " + ((JPanelPoint)source).getLocation().getX() 
							+ "," + ((JPanelPoint)source).getLocation().getY() + " clicked");
		//Draws coordinate information in upper panel
		scheduler_.showStats(((JPanelPoint)source).getLocation().getX(), ((JPanelPoint)source).getLocation().getY());
	}
	
	private void levelSelectionClick(Object source) {
		if (openLevels_ >= ((JPanelLevel)source).getLevelNumber()) { //Checks if level is unlocked
			if (((JPanelLevel)source).getLevelNumber() < openLevels_) { //checks if level was already cleared
				playingOldLevel_ = true;
			}else {
				playingOldLevel_ = false;
			}
			lvlUI_.setVisible(false);
			GamingGUI origGUI = new GamingGUI(this);
			gamingUI_ = origGUI;
			GUILauraInterface lauraGUI = origGUI;
			GUICoordinateInterface coordinateGUI = origGUI;
			if (scheduler_.configurateLevel(((JPanelLevel)source).getLevelNumber(), lauraGUI, 
					coordinateGUI)) {
				//LevelConfig succeeded
			}else {
				//LevelConfig failed
				scheduler_.clearLevel();
				//if level file is corrupted next level will be opened
				++openLevels_;
				lvlUI_.hideText();
				if (openLevels_ > 7) 
					System.out.println("GAME WON! CONGRATULATIONS!");
			}
		}else { //clicked level that is not open yet
			System.out.println("Inaccessible level clicked");
			lvlUI_.notifyLevelLocked();
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		invariant();
		try {
			if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				System.out.println("Right pressed.");
				scheduler_.actLaura(Direction.RIGHT);
			} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				System.out.println("Left pressed.");
				scheduler_.actLaura(Direction.LEFT);
			} else if (e.getKeyCode() == KeyEvent.VK_UP) {
				System.out.println("Up pressed.");
				scheduler_.actLaura(Direction.UP);
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				System.out.println("Down pressed.");
				scheduler_.actLaura(Direction.DOWN);
			} else {
				System.out.println("Key pressed: " + e.getKeyChar());
				if (e.getKeyChar() == 'w'){
					Laura.getLaura().shoot(Direction.UP);
				} else if (e.getKeyChar() == 'a'){
					Laura.getLaura().shoot(Direction.LEFT);
				} else if (e.getKeyChar() == 's'){
					Laura.getLaura().shoot(Direction.DOWN);
				} else if (e.getKeyChar() == 'd'){
					Laura.getLaura().shoot(Direction.RIGHT);
				}
			}
		}catch(LevelCompletedException exep) {
			if (!playingOldLevel_) //if level was already won, openLevels_ doesn't increase
				++openLevels_;
			lvlUI_.hideText();
			changeLevel(); //clears existing level and opens levelUI
			if (openLevels_ > 7)
				System.out.println("GAME WON! CONGRATULATIONS!");
			return;
		}
		if (!Laura.getLaura().movesLeft()) {
			endTurn();
		}
		invariant();
	}
	
	private void endTurn() {
		scheduler_.restoreCreatures();
		for (int i = 1; i < scheduler_.countCreatures(); ++i) {
			scheduler_.actCreature(i);
		}
		Laura.getLaura().restoreMoves();
		Laura.getLaura().drawStats();
		if (Laura.getLaura().isDead()) {
			lauraDies();
		}
	}


	@Override
	public void actionPerformed(ActionEvent e) { //aka button listener
		Object source = e.getSource();
		if (source instanceof JButtonInventory) { //inventory
			inventoryButtonClicked(source);
		}else if (source instanceof JButtonLevel) { //Start and levelMenu buttons
			menuButtonClicked(source);
		}
		invariant();
	}
	
	private void inventoryButtonClicked(Object source) {
		if (((JButtonInventory) source).getName() == "Menu") {
			changeLevel();
			return;
		}
		else if (lastInventoryClicked_ != null) {
			System.out.println(((JButtonInventory) source).getName() 
					+ " button pressed with inventory " 
					+ lastInventoryClicked_.getOrderFromTop() 
					+ " selected");
			scheduler_.organizeInventory(source, lastInventoryClicked_);
			if (((JButtonInventory) source).getName() == "Drop") {
				gamingUI_.drawBorder(lastInventoryClicked_.getOrderFromTop(), false);
				lastInventoryClicked_ = null;
			}
		}else {
			System.out.println(((JButtonInventory) source).getName() 
								+ " button pressed with no inventory selection");
		}
		gamingUI_.setRequestFocusInWindow();
	}
	
	private void menuButtonClicked(Object source) {
		if (((JButtonLevel) source).getName().equals("Save")) { //levelMenu
			System.out.println("Save button clicked");
			SaveGame saveGame = new SaveGame();
			try {
				saveGame.save(openLevels_);
				lvlUI_.printGameSaved(true);
			}catch (IOException ex) {
	            System.err.println("ERROR: Save game failed.");
				lvlUI_.printGameSaved(false);
			}
		}else if(((JButtonLevel) source).getName().equals("New Game") || //StartMenu
				((JButtonLevel) source).getName().equals("Load Game")) {
			menuUI_.setVisible(false); //hide menu
			//interface for config
			GEConfigInterface GEConfInt = (GEConfigInterface)this;
			if (((JButtonLevel) source).getName().equals("New Game")) {
				Config conf = new Config(true, GEConfInt); //true if new game
				conf.configurateGame(); //initializes laura and her items
			}else {
				Config conf = new Config(false, GEConfInt); //load game
				conf.configurateGame();
			}
			lvlUI_ = new LevelUI((GameEngineInterface)this);
		}else if(((JButtonLevel) source).getName().equals("Options")) {
			//Show options menu
			//TODO
		}else {
			System.err.println("ERROR: Invalid JButtonLevel");
		}
	}
	
	private void changeLevel() {
		invariant();
		gamingUI_.setVisible(false);
		gamingUI_ = null;
		scheduler_.clearLevel();
		lastInventoryClicked_ = null;
		lvlUI_.setVisible(true);
		invariant();
	}
	
	private void lauraDies() {
		Laura.getLaura().clearItems();
		Laura.getLaura().setHealth(Laura.getLaura().getMaxHealth());
		changeLevel();
		invariant();
	}
	
	private void invariant() {
		assert openLevels_ >= 1 && openLevels_ <= 8;
		assert scheduler_ != null;
		assert lvlUI_ != null;
		assert menuUI_ != null;
	}
	
	
	
	
	
	//Extra methods to satisfy implemented classes
	//Java doesn't like multiple extends :(
	@Override
	public void keyTyped(KeyEvent e) {
		//Do nothing
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		//Do nothing
	}

}
