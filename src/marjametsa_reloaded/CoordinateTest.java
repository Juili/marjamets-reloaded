package marjametsa_reloaded;

import static org.junit.Assert.*;

import org.junit.Test;

public class CoordinateTest {

	@Test
	public void testGetX() {
		Coordinate test = new Coordinate(1,2,Terrain.DIRT);
		assertEquals(1, test.getX());
		
		test = new Coordinate(1,2);
		assertEquals(1, test.getX());
	}

	@Test
	public void testGetY() {
		Coordinate test = new Coordinate(1,2,Terrain.DIRT);
		assertEquals(2, test.getY());
		
		test = new Coordinate(1,2);
		assertEquals(2, test.getY());
	}

	@Test
	public void testGetTerrain() {
		Coordinate test = new Coordinate(1,1,Terrain.DIRT);
		assertEquals(Terrain.DIRT, test.getTerrain());
		
		test = new Coordinate(1,1);
		assertEquals(Terrain.GRASS, test.getTerrain());
	}

	@Test
	public void testIsObstacle() {
		Coordinate test = new Coordinate(1,1);
		assertFalse(test.isObstacle());
		
		test.setObstacle(Obstacle.BOULDER);
		assertTrue(test.isObstacle());
	}

	@Test
	public void testGetDetail() {
		Coordinate test = new Coordinate(1,1);
		assertEquals(Detail.EMPTY, test.getDetail());
		
		test.setDetail(Detail.BLOOD);
		assertEquals(Detail.BLOOD, test.getDetail());
	}

	@Test
	public void testGetCreature() {
		Coordinate test = new Coordinate(1,1);
		assertNull(test.getCreature());
		
		try {
			test.setCreature(new Bear());
			assertNotNull(test.getCreature());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetItem() {
		Coordinate test = new Coordinate(1,1);
		assertNull(test.getItem());
		
		try {
			test.addItem(new Weapon(12, "Axe", 10, true));
			assertNotNull(test.getItem());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSetTerrain() {
		Coordinate test = new Coordinate(1,1);
		test.setTerrain(Terrain.DIRT);
		assertEquals(Terrain.DIRT, test.getTerrain());
		
		test.setTerrain(Terrain.GRASS);
		assertEquals(Terrain.GRASS, test.getTerrain());
	}

	@Test
	public void testSetObstacle() {
		Coordinate test = new Coordinate(1,1,Terrain.DIRT);
		assertEquals(Obstacle.EMPTY, test.getObstacle());
		
		test = new Coordinate(1,1);
		assertEquals(Obstacle.EMPTY, test.getObstacle());
		
		test.setObstacle(Obstacle.BOULDER);
		assertEquals(Obstacle.BOULDER, test.getObstacle());
		
		test.setObstacle(Obstacle.EXIT);
		assertEquals(Obstacle.EXIT, test.getObstacle());
		
		test.setObstacle(Obstacle.TREE);
		assertEquals(Obstacle.TREE, test.getObstacle());
		
		test.setObstacle(Obstacle.EMPTY);
		assertEquals(Obstacle.EMPTY, test.getObstacle());
	}

	@Test
	public void testSetDetail() {
		Coordinate test = new Coordinate(1,1);
		assertEquals(Detail.EMPTY, test.getDetail());
		
		test.setDetail(Detail.BLOOD);
		assertEquals(Detail.BLOOD, test.getDetail());
		
		test.setDetail(Detail.EMPTY);
		assertEquals(Detail.EMPTY, test.getDetail());
	}
	
	@Test(expected = NullPointerException.class)
	public void testDrawAll() {
		Coordinate test = new Coordinate(1,1);
		test.drawAll();
	}

	@Test(expected = NullPointerException.class)
	public void testDrawBackground() {
		Coordinate test = new Coordinate(1,1);
		test.drawBackground();
	}

	@Test(expected = NullPointerException.class)
	public void testDrawObstacle() {
		Coordinate test = new Coordinate(1,1);
		test.setObstacle(Obstacle.BOULDER);
		test.drawObstacle();
	}

	@Test(expected = NullPointerException.class)
	public void testDrawDetail() {
		Coordinate test = new Coordinate(1,1);
		test.setDetail(Detail.BLOOD);
		test.drawDetail();
	}

	@Test(expected = NullPointerException.class)
	public void testDrawCreature() {
		Coordinate test = new Coordinate(1,1);
		test.addCreature(new Bear());
		test.drawCreature();
	}

	@Test(expected = NullPointerException.class)
	public void testDrawLaura() {
		Coordinate test = new Coordinate(1,1);
		test.addCreature(Laura.getLaura());
		test.drawLaura();
	}

	@Test(expected = NullPointerException.class)
	public void testDrawLauraDirection() {
		Coordinate test = new Coordinate(1,1);
		test.addCreature(Laura.getLaura());
		test.drawLaura(Direction.DOWN);
	}

	@Test(expected = NullPointerException.class)
	public void testDrawItem() {
		Coordinate test = new Coordinate(1,1);
		test.addItem(new Weapon(10, "Branch", 5, false));
		test.drawItem();
	}

	@Test(expected = NullPointerException.class)
	public void testAddCreature() {
		Coordinate test = new Coordinate(1,1);
		test.addCreature(new Bear());
	}

	@Test(expected = NullPointerException.class)
	public void testRemoveCreature() {
		Coordinate test = new Coordinate(1,1);
		assertNull(test.getCreature());
		test.removeCreature();
		assertNull(test.getCreature());
		
		try {
			Creature testBear = new Bear();
			testBear.setHealth(10);
			test.setCreature(testBear);
			assertNotNull(test.getCreature());
			test.removeCreature();
			assertNull(test.getCreature());
			testBear.setHealth(0);
			test.setCreature(testBear);
			assertNotNull(test.getCreature());
			test.removeCreature();
			assertNull(test.getCreature());
		}catch (NullPointerException ex) {
			throw new NullPointerException();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testRemoveObstacle() {
		Coordinate test = new Coordinate(1,1,Terrain.DIRT);
		assertEquals(Obstacle.EMPTY, test.getObstacle());
		try {
			test.removeObstacle();
		}catch(NullPointerException e) {
			assertEquals(Obstacle.EMPTY, test.getObstacle());
		}
		try {
			test.setObstacle(Obstacle.BOULDER);
			assertEquals(Obstacle.BOULDER, test.getObstacle());
			
			test.removeObstacle();
		}catch(NullPointerException e) {
			assertEquals(Obstacle.EMPTY, test.getObstacle());
		}
		try {
			test.removeObstacle();
		}catch(NullPointerException e) {
			assertEquals(Obstacle.EMPTY, test.getObstacle());
		}
	}

	@Test
	public void testSetItem() {
		Coordinate test = new Coordinate(1,1);
		assertNull(test.getItem());
		test.addItem(null);
		assertNull(test.getItem());
		
		test.addItem(new Weapon(10, "Branch", 5, false));
		assertNotNull(test.getItem());
	}

	@Test
	public void testRemoveItem() {
		Coordinate test = new Coordinate(1,1);
		try {
			test.addItem(null);
			test.removeItem();
			assertNull(test.getItem());
			
			test.addItem(new Weapon(10, "Branch", 5, false));
			test.removeItem();
			assertNotNull(test.getItem());
		}catch (NullPointerException e) {
			assertNull(test.getItem());
		}
		try {
			test.addItem(new Weapon(10, "Branch", 5, false));
			test.removeItem();
		}catch(NullPointerException e) {
			assertNotNull(test.getItem());
		}
	}

	@Test(expected = NullPointerException.class)
	public void testDrawStats() {
		Coordinate test = new Coordinate(1,1);
		test.drawStats();
	}

	@Test
	public void testIsCreature() {
		Coordinate test = new Coordinate(1,1);
		assertFalse(test.isCreature());
		try {
			Creature testBear = new Bear();
			testBear.setHealth(10);
			test.setCreature(testBear);
			assertTrue(test.isCreature());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testIsItem() {
		Coordinate test = new Coordinate(1,1);
		assertFalse(test.isItem());
		test.addItem(new Weapon(10, "Branch", 5, false));
		assertTrue(test.isItem());
	}

	@Test
	public void testIsTree() {
		Coordinate test = new Coordinate(1,1);
		assertFalse(test.isTree());
		test.setObstacle(Obstacle.TREE);
		assertTrue(test.isTree());
	}

	@Test
	public void testIsExit() {
		Coordinate test = new Coordinate(1,1);
		assertFalse(test.isExit());
		test.setObstacle(Obstacle.EXIT);
		assertTrue(test.isExit());
	}

	@Test(expected = NullPointerException.class)
	public void testDrawShotgunDetail() {
		Coordinate test = new Coordinate(1,1);
		test.drawShotgunDetail(Direction.DOWN, "NEAR");
	}

}
