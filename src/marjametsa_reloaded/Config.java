package marjametsa_reloaded;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

enum ConfigResult {SUCCESS, FAILURE};

public class Config {
	
	GEConfigInterface GE_;
	Factory factory_;
	private boolean isNewGame_;
	private static String newGameFilename_ = "newGame.txt";
	private static String loadGameFilename_ = "loadGame.txt";
	private static int lastFoundRow_ = 0;
	private final static Charset ENCODING = StandardCharsets.UTF_8;
	private List<String> lines_;
	private Path path_;
	
	public Config(boolean newGame, GEConfigInterface GE) throws OutOfMemoryError {
		isNewGame_ = newGame;
		GE_ = GE;
		factory_ = new Factory();
		try {
			lines_ = openFile();
			if (lines_.size() == 0) {
				isNewGame_ = true;
				lines_ = openFile();
			}
		} catch (IOException e) {
			System.err.println("Error: Could not find " + path_.toUri());
			System.exit(1); //shuts application, because config could not run properly
		}
	}
	
	//"main" for config
	public ConfigResult configurateGame() throws OutOfMemoryError {
		if (createLaura() == ConfigResult.SUCCESS) {
			GE_.setOpenLevels(getOpenLevels());
			while (createWeapon() == ConfigResult.SUCCESS);
			while (createPotion() == ConfigResult.SUCCESS);
			System.out.println("Laura and her items configurated");
			return ConfigResult.SUCCESS;
		}
		
		return ConfigResult.FAILURE;
	}
	
	//Sets lauras initial stats
	private ConfigResult createLaura() {
		String line = "";
		String sublines[];
		for (int i = 1; i < lines_.size(); ++i) {
			sublines = null;
			line = lines_.get(i);
			sublines = line.split(":");
			if (sublines[0].equals("L")) {
				sublines = separateByComma(sublines);
				for (int j = 0; j < sublines.length; ++j) {
					switch(j) {
					case 0:
						Laura.getLaura().setHealth(Integer.parseInt(sublines[j]));
						continue;
					case 1:
						Laura.getLaura().setMaxHealth((Integer.parseInt(sublines[j])));
						continue;
					case 2:
						Laura.getLaura().setSpeed(Integer.parseInt(sublines[j]));
						Laura.getLaura().restoreMoves();
						continue;
					case 3: 
						Laura.getLaura().setStrength(Integer.parseInt(sublines[j]));
						continue;
					case 4:
						Laura.getLaura().setDefense(Integer.parseInt(sublines[j]));
						continue;
					case 5:
						Laura.getLaura().setName(sublines[j]);
						return ConfigResult.SUCCESS;
					default:
						System.err.println("Error: file corrupted");
						System.exit(1);
					}
				}
				break;
			}
		}
		System.err.println("Error: file corrupted");
		System.exit(1);
		return ConfigResult.FAILURE; //Never reaches this
	}
	
	//sets open levels in Game Engine
	private int getOpenLevels() {
		String line = "";
		String sublines[];
		for (int i = 1; i < lines_.size(); ++i) {
			sublines = null;
			line = lines_.get(i);
			sublines = line.split(":");
			if (sublines[0].equals("O")) {
				return Integer.parseInt(sublines[1]);
			}
		}
		System.err.println("Error: file corrupted");
		System.exit(1);
		return -1; //program never gets here, added to be able to compile
	}
	
	//Creates weapon and adds it to Laura's inventory
	private ConfigResult createWeapon() throws OutOfMemoryError {
		String line = "";
		String sublines[];
		Type wtype;
		while (lastFoundRow_ < lines_.size()) {
			wtype = new ItemType();
			sublines = null;
			line = lines_.get(lastFoundRow_);
			sublines = line.split(":");
			if (sublines[0].equals("W")) {
				sublines = separateByComma(sublines);
				for (int j = 0; j < sublines.length; ++j) {
					switch(j) {
					case 0:
						((ItemType)wtype).setId(Integer.parseInt(sublines[j]));
						continue;
					case 1:
						((ItemType)wtype).setName(sublines[j]);
						continue;
					case 2:
						((ItemType)wtype).setDamage(Integer.parseInt(sublines[j]));
						continue;
					case 3:
						((ItemType)wtype).setCutsWood(Boolean.parseBoolean(sublines[j]));
						++lastFoundRow_;
						if (factory_.createLauraItem(wtype) == FactoryResult.FAILURE) {
							System.err.println("Error: Failed to create weapon");
							System.exit(1);
						}
						return ConfigResult.SUCCESS;
					default:
						System.err.println("Error: file corrupted");
						System.exit(1);
					}
				}
			}
			++lastFoundRow_;
		}
		lastFoundRow_ = 0;
		return ConfigResult.FAILURE;
	}
	
	//creates potion and adds it to Laura's inventory
	private ConfigResult createPotion() throws OutOfMemoryError {
		String line = "";
		String sublines[];
		Type ptype;
		while (lastFoundRow_ < lines_.size()) {
			ptype = new ItemType();
			sublines = null;
			line = lines_.get(lastFoundRow_);
			sublines = line.split(":");
			if (sublines[0].equals("P")) {
				sublines = separateByComma(sublines);
				for (int j = 0; j < sublines.length; ++j) {
					switch(j) {
					case 0:
						((ItemType)ptype).setId(Integer.parseInt(sublines[j]));
						continue;
					case 1:
						((ItemType)ptype).setName(sublines[j]);
						continue;
					case 2:
						((ItemType)ptype).setHealingValue(Integer.parseInt(sublines[j]));
						++lastFoundRow_;
						if (factory_.createLauraItem(ptype) == FactoryResult.FAILURE) {
							System.err.println("Error: Failed to create potion");
							System.exit(1);
						}
						return ConfigResult.SUCCESS;
					default:
						System.err.println("Error: file corrupted");
						System.exit(1);
					}
				}
			}
			++lastFoundRow_;
		}
		lastFoundRow_ = 0;
		return ConfigResult.FAILURE;
	}
	
	//Reads file to memory
	private List<String> openFile() throws IOException {
		if (isNewGame_) {
			path_ = FileSystems.getDefault().getPath("configFiles", newGameFilename_);
			System.out.println("Reads " + path_.toUri());
		} else {
			path_ = FileSystems.getDefault().getPath("configFiles", loadGameFilename_);
			System.out.println("Reads " + path_.toUri());
		}
		return Files.readAllLines(path_, ENCODING);
	}
	
	//Splits line by comma
	private String[] separateByComma(String[] attributes) {
		String line = attributes[1];
		attributes = null;
		attributes = line.split(",");
		return attributes;
	}
}
