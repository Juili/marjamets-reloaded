package marjametsa_reloaded;

import java.util.Vector;

public class LevelScheduler implements LSFactoryInterface, LSLauraInterface {
	//Vector for creatures
	private static Vector<Creature> creatures_;
	
	//y<x<coordinate>>
	private Vector<Vector<Coordinate>> coordinates_;
	private Vector<Coordinate> coordinateRow_;
	
	private int maxX_;
	private int maxY_;
	
	public LevelScheduler() {
		System.out.println("LeverScheduler created");
		//coordinateGUI_ = coordinateGUI;
		creatures_ = new Vector<Creature>();
		coordinates_ = new Vector<Vector<Coordinate>>();
		coordinateRow_ = new Vector<Coordinate>();
		Laura.getLaura().setLSInterface((LSLauraInterface)this);
	}
	
	@SuppressWarnings("unchecked")
	public boolean addCoordinate(int x, int y, Coordinate coordinate){
		if (coordinate != null || y > maxY_) {
			if (x < maxX_) {
				coordinateRow_.add(coordinate);
				return true;
			}else if (x == maxX_) {
				coordinateRow_.add(coordinate);
				coordinates_.add(y, coordinateRow_);
				coordinates_.set(y, (Vector<Coordinate>)coordinateRow_.clone());
				coordinateRow_.clear();
				return true;
			}
		}
		return false;
	}
	
	public Coordinate getCoordinate(int x, int y) {
		invariant();
		try {
			return coordinates_.get(y).get(x);
		}catch(IndexOutOfBoundsException e) {
			return null;
		}catch (NullPointerException n) {
			return null;
		}
	}
	
	//Function for adding an existing creature to a coordinate
	//Factory doesn't use this
	public boolean addCreature(int x, int y, Creature cre){
		invariant();
		if(! coordinates_.get(y).get(x).addCreature(cre)){
			System.err.println("Cannot add creature");
			return false;
		}
		return true;
	}
	
	//Function for removing creature from coordinate
	public boolean removeCreature(int x, int y){
		invariant();
		try {
			return(coordinates_.get(y).get(x).removeCreature());
		}catch(IndexOutOfBoundsException ex) {
			System.err.println("ERROR: Invalid coordinate to removeCreature.");
			return false;
		}
	}
	
	//Function for adding Laura into the level.
	public boolean addLaura(Laura L){
		if (L != null) {
			creatures_.add(L); //this is the first creature to be added
			assert creatures_.size() == 1;
			return true;
		}
		return false;
	}
	
	public int getLauraX(){
		invariant();
		return creatures_.get(0).getX();
	}
	
	public int getLauraY(){
		invariant();
		return creatures_.get(0).getY();
	}
	
	public Creature getCreature(int i){
		invariant();
		if(i < countCreatures() && i >= 0){
			return creatures_.get(i);
		}
		
		System.err.println("ERROR: Bad index for creature");
		return null;
	}
	
	//Function for checking if the coordinate of parameters is allowed to move
	public boolean ableToMove(int x, int y){
		invariant();
		
		//If the coordinate would be out of bounds
		if(x < 0 || y < 0 || x > 16 || y > 11 ){
			System.out.println("Reached boundary!");
			return false;
		}
		else if(coordinates_.get(y).get(x).isObstacle()){
			return false;
		}
		else if(coordinates_.get(y).get(x).isCreature()){
			return false;
		}
		
		return true;
	}
	
	//Get the parameter for direction next to x and y
	public Coordinate coordinateAtDirection(int x, int y, Direction dir){ //TODO
		invariant();
		
		if(dir == Direction.UP)
			--y;
		else if(dir == Direction.RIGHT)
			++x;
		else if(dir == Direction.DOWN)
			++y;
		else if(dir == Direction.LEFT)
			--x;
		
		//If the coordinate would be out of bounds
		if(x < 0 || y < 0 || x > 16 || y > 11 ){
			System.out.println("Reached boundary!");
			invariant();
			return null;
		}
		invariant();
		return coordinates_.get(y).get(x);
	}
	
	//Function for creature to act
	public boolean actCreature(int index){
		invariant();
		if(index == 0){
			System.err.println("ERROR: Tried to actCreature on Laura");
			return false;
		}
		
		//Let's remove dead creatures from creatures_
		if(creatures_.get(index).isDead()){
			System.out.println("Creature died");
			creatures_.remove(index);
			invariant();
			return true;
		}
		return creatures_.get(index).act(this);
	}
	
	public boolean actLaura(Direction dir) throws LevelCompletedException {
		invariant();
		if(!Laura.getLaura().movesLeft()){
			System.out.println("Laura doesn't have moves left!");
			return false;
		}
		
		else if(Laura.getLaura().act(dir)){
			return true;			
		}
		return false;
	}
	
	public boolean addCreature(Creature creature) {
		invariant();
		if (creature != null) {
			creatures_.add(creature);
			invariant();
			return true;
		}
		return false;
	}
	
	public boolean configurateLevel(int levelNumber, GUILauraInterface lauraGUI, 
									GUICoordinateInterface coordinateGUI) {
		//Create interface of this instance and pass it to level config builder
		LSFactoryInterface LSInt = (LSFactoryInterface) this;
		LevelConfig LCInt = new LevelConfig(LSInt, levelNumber, coordinateGUI, lauraGUI);
		
		//set specified level
		if (LCInt.setLevel() == LevelConfiguration.SUCCESS) {
			System.out.println("Level configuration successful");
			//Draws Laura's initial items in inventory
			Laura.getLaura().drawItems();
			Laura.getLaura().drawStats();
			for (int outer = 0; outer < coordinates_.size(); ++outer) {
				for (int inner = 0; inner < coordinates_.get(outer).size(); ++inner) {
					coordinates_.get(outer).get(inner).drawAll();
				}
			}
			invariant();
			return true;
		}
		System.err.println("ERROR: Level configuration failed");
		return false;
	}
	
	//Deletes Lauras item or sets it in use
	public void organizeInventory(Object source, JPanelInventory clickedObj) {
		invariant();
		if (((JButtonInventory) source).getName() == "Drop") {
			try {
				if (Laura.getLaura().getItem(clickedObj.getOrderFromTop() - 1) == 
						Laura.getLaura().getItemInUse()) {
					Laura.getLaura().removeItemInUse();
				}
				Laura.getLaura().deleteItem(clickedObj.getOrderFromTop() - 1);
				System.out.println("Item dropped");
			}catch (ArrayIndexOutOfBoundsException ex) {
				System.out.println("No item dropped");
			}	
		}else if (((JButtonInventory)source).getName() == "Equip") {
			try {
				Laura.getLaura().setItemInUse(Laura.getLaura().getItem(clickedObj.getOrderFromTop() - 1), 
						clickedObj.getOrderFromTop() - 1);
				System.out.println("Item equipped");
			}catch(ArrayIndexOutOfBoundsException ex) {
				Laura.getLaura().removeItemInUse();
				System.out.println("No item equipped");
			}
		}
		Laura.getLaura().drawItems();
		invariant();
	}
	
	public void setLevelMaxSize(int maxX, int maxY) {
		maxX_ = maxX;
		maxY_ = maxY;
		assert maxX_ == maxX && maxY_ == maxY;
	}
	
	public int getMaxX() {
		assert maxX_ > 0;
		return maxX_;
	}
	
	public int getMaxY() {
		assert maxY_ > 0;
		return maxY_;
	}

	public void showStats(double x, double y) {
		invariant();
		coordinates_.get((int) y).get((int) x).drawStats();
	}
	
	public int countCreatures(){
		invariant();
		return creatures_.size();
	}
	
	public void restoreCreatures(){
		invariant();
		for (int i = 1; i < creatures_.size(); ++i){
			creatures_.get(i).restoreMoves();
		}
		invariant();
	}
	
	public void clearLevel() {
		invariant();
		clearCreatures();
		clearCoordinates();
		assert creatures_.isEmpty();
		assert coordinates_.isEmpty();
	}
	
	private void clearCreatures() {
		creatures_.clear();
		System.out.println("Creatures cleared");
	}
	
	private void clearCoordinates() {
		coordinates_.clear();
		coordinateRow_.clear();
		System.out.println("Coordinates cleared");
	}
	
	private void invariant() {
		assert creatures_.size() >= 1;
		assert maxX_ > 0;
		assert maxY_ > 0;
		assert coordinates_.size() == maxY_ + 1; //coordinates start from zero
		for (int i = 0; i < coordinates_.size(); ++i) {
			assert coordinates_.get(i).size() == maxX_ + 1;
		}
	}
	
}
