package marjametsa_reloaded;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;

public class GamingGUI extends JFrame implements GUIGEInterface, GUILauraInterface, 
												GUICoordinateInterface {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane_;
	private JPanel mainArea_;
	
	private JPanel itemInUse_;
	
	private Dimension pictureSize_ = new Dimension(75,75);
	private Dimension mapTerrainSize_ = new Dimension(50,50);
	
	private BufferedImage upperBackground_ = null;
	private BufferedImage sideBackground_ = null;
	private BufferedImage mainBackground_ = null;
	
	//Listeners
	private KeyListener GEKey_;
	private MouseListener GEMouse_;
	private ActionListener GEButton_;
	
	// UpperPanel values
	//Laura
	private JProgressBar lhealthBar;
	private JTextField lstrength;
	private JTextField ldefense;
	private JTextField lspeed;
	
	//Creature
	private JPanelWithBackground cimage;
	private JTextField cname;
	private JTextField cspeed;
	private JTextField cstrength;
	private JTextField cdefense;
	private JProgressBar chealthBar;
	
	//sidePanel
	Map<Integer, JPanelInventory> inventory_;
	
	//mainArea_ values
	private Vector<Vector<JPanel>> vertical = null;
	private Vector<JPanel> horizontal = null;
	
	final int MAX_horizontalMapSize = 17;
	final int MAX_verticalMapSize = 12;
	
	private int horizontalMapSize = 17;
	private int verticalMapSize = 12;
	
	private int horizontalSize = horizontalMapSize * (int) mapTerrainSize_.getWidth();
	private int verticalSize = verticalMapSize * (int) mapTerrainSize_.getHeight();
	
	private Dimension mapSize = new Dimension(horizontalSize,verticalSize);

	/**
	 * Create the frame.
	 */
	public GamingGUI(GameEngineInterface gei) {
		GEKey_ = (KeyListener)gei;
		GEMouse_ = (MouseListener)gei;
		GEButton_ = (ActionListener)gei;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1024, 768);
		setLocationRelativeTo(null);
		setResizable(false);
		contentPane_ = new JPanel();
		contentPane_.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane_.setLayout(new BorderLayout(0, 0));
		contentPane_.addKeyListener(GEKey_);
		setContentPane(contentPane_);
		
//		Configuration of upperPanel
		try{
			upperBackground_ = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("UPPER")));
		} catch (Exception ex){
			System.err.println("ERROR: upperPanelbackground file not found.");
		}
		JPanel upperPanel = new JPanelWithBackground(upperBackground_);
		upperPanel.setPreferredSize(new Dimension(1024,100));
		upperPanel.setBorder(null);
		contentPane_.add(upperPanel, BorderLayout.NORTH);
		
		try{
			JPanel limage = new JPanelWithBackground(ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("LAURA"))));
			limage.setPreferredSize(pictureSize_);
			
			lhealthBar = new JProgressBar(0,0);
			
			JTextField lname = new JTextField("Laura");
			lname.setFont(new Font("Tahoma", Font.BOLD, 14));
			lname.setEditable(false);
			lname.setOpaque(false);
			lname.setBorder(null);
			
			lspeed = new JTextField(null);
			lspeed.setColumns(10);
			lspeed.setFont(new Font("Tahoma", Font.BOLD, 12));
			lspeed.setEditable(false);
			lspeed.setOpaque(false);
			lspeed.setBorder(null);
			
			lstrength = new JTextField(null);
			lstrength.setColumns(6);
			lstrength.setFont(new Font("Tahoma", Font.BOLD, 12));
			lstrength.setEditable(false);
			lstrength.setOpaque(false);
			lstrength.setBorder(null);
			
			ldefense = new JTextField(null);
			ldefense.setColumns(6);
			ldefense.setFont(new Font("Tahoma", Font.BOLD, 12));
			ldefense.setEditable(false);
			ldefense.setOpaque(false);
			ldefense.setBorder(null);
			
			cimage = new JPanelWithBackground(ImageIO.read(new File(
											ImageLocations.getImageLocations().getFilePath("-1"))));
			cimage.setPreferredSize(pictureSize_);
			
			chealthBar = new JProgressBar(0,0);
			chealthBar.setVisible(false);
			
			cname = new JTextField(null);
			cname.setEditable(false);
			cname.setOpaque(true);
			cname.setColumns(20);
			cname.setBorder(null);
			cname.setVisible(false);
			cname.setFont(new Font("Tahoma", Font.BOLD, 12));
			
			cspeed = new JTextField(null);
			cspeed.setBorder(null);
			cspeed.setFont(new Font("Tahoma", Font.BOLD, 12));
			
			cstrength = new JTextField(null);
			cstrength.setEditable(false);
			cstrength.setOpaque(true);
			cstrength.setColumns(7);
			cstrength.setBorder(null);
			cstrength.setVisible(false);
			cstrength.setFont(new Font("Tahoma", Font.BOLD, 12));
			
			cdefense = new JTextField(null);
			cdefense.setBorder(null);
			cdefense.setFont(new Font("Tahoma", Font.BOLD, 12));
			
			GroupLayout gl_upperPanel = new GroupLayout(upperPanel);
			gl_upperPanel.setHorizontalGroup(
				gl_upperPanel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_upperPanel.createSequentialGroup()
						.addGap(23)
						.addComponent(limage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_upperPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(lhealthBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_upperPanel.createSequentialGroup()
								.addComponent(lstrength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(ldefense, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lspeed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(142)
						.addComponent(cimage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_upperPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(chealthBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(cname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_upperPanel.createSequentialGroup()
								.addComponent(cstrength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(cdefense, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(cspeed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(225, Short.MAX_VALUE))
			);
			gl_upperPanel.setVerticalGroup(
				gl_upperPanel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_upperPanel.createSequentialGroup()
						.addGap(5)
						.addGroup(gl_upperPanel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_upperPanel.createSequentialGroup()
								.addGap(5)
								.addComponent(cname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(chealthBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_upperPanel.createParallelGroup(Alignment.BASELINE)
									.addComponent(cstrength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(cdefense, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(cspeed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_upperPanel.createSequentialGroup()
								.addGap(5)
								.addComponent(lname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lhealthBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_upperPanel.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_upperPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(ldefense, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lspeed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addComponent(lstrength, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addComponent(limage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(cimage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(26))
			);
			upperPanel.setLayout(gl_upperPanel);
			
			} catch (Exception ex){
				System.err.println("ERROR: Problem loading stats.");
			}
		
//		SidePanels configuration

		try{
			sideBackground_ = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("SIDE")));
		} catch (Exception ex){
			System.err.println("ERROR: sidePanelbackground file not found.");
		}
		
		JPanel sidePanel = new JPanelWithBackground(sideBackground_);
		sidePanel.setPreferredSize(new Dimension (140, 668));
		sidePanel.setBorder(null);
		contentPane_.add(sidePanel, BorderLayout.WEST);
		
		try{
			itemInUse_ = new JPanelWithBackground(null);
			itemInUse_.setPreferredSize(pictureSize_);
			
			JLabel lblEquipped = new JLabel("    Equipped    ");
			JButton btnEquip = new JButtonInventory("Equip");
			JButton btnDrop = new JButtonInventory("Drop");
			JButton btnBack = new JButtonInventory("Menu");
			
			btnEquip.addActionListener(GEButton_);
			btnDrop.addActionListener(GEButton_);
			btnBack.addActionListener(GEButton_);
			
			sidePanel.add(itemInUse_);
			sidePanel.add(lblEquipped);
			sidePanel.add(btnEquip);
			sidePanel.add(btnDrop);
			
			inventory_ = new HashMap<Integer, JPanelInventory>();
			for(int i = 1; i <= 6; ++i){
				inventory_.put(i, new JPanelInventory(null, i));
				inventory_.get(i).setPreferredSize(pictureSize_);
				inventory_.get(i).addMouseListener(GEMouse_);
				sidePanel.add(inventory_.get(i));
			}
			sidePanel.add(btnBack);
			btnBack.setVisible(false); //This button is only for testing and debugging purposes

		} catch (Exception ex){
			System.err.println("ERROR: sidePanelbackground file not found.");
		}
		
//		mainArea_s configuration
		try{
			mainBackground_ = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("MAIN")));
			mainArea_ = new JPanelWithBackground(mainBackground_);
			contentPane_.add(mainArea_, BorderLayout.CENTER);		
			
			vertical = new Vector<Vector<JPanel>>();
			for(int i = 0; i <= verticalMapSize-1; ++i){
				horizontal = new Vector<JPanel>();
				for(int j = 0; j <= horizontalMapSize-1; ++j){
					JPanel column = new JPanelPoint (null, j, i);
					column.addMouseListener(GEMouse_);
					horizontal.add(column);
				}
				vertical.add(horizontal);
			}
			drawMap();
		} catch (Exception ex){
			System.err.println("ERROR: mainBackground file not found.");
		}
		
//		Set everything visible
		this.setVisible(true);
		contentPane_.requestFocusInWindow();
	}
	
	public void setRequestFocusInWindow() {
		contentPane_.requestFocusInWindow();
	}
	
	public void drawItemInUse(int id) {
		try {
			BufferedImage image = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath(Integer.toString(id))));
			((JPanelWithBackground)itemInUse_).setBackground(image);
		}catch(IOException e) {
			System.err.println("ERROR: ItemInUse file not found.");
		}
	}
	
	public void drawMap(){
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setPreferredSize(mapSize);
		mainArea_.add(verticalBox);
		for(int i = 0; i < vertical.size(); ++i){
			Box horizontalBox = Box.createHorizontalBox();
			horizontalBox.setPreferredSize(mapSize);
			verticalBox.add(horizontalBox);
			for(int j = 0; j < horizontal.size(); ++j){
				horizontalBox.add(vertical.get(i).get(j));
			}
		}
	}
	
	//Draws one inventory item based on order and item id
	private void drawInventoryItem(int id, int orderFromTop) {
		try {
			BufferedImage image = ImageIO.read(new File(
							ImageLocations.getImageLocations().getFilePath(Integer.toString(id))));
			if(1<=orderFromTop || orderFromTop<=6)
				((JPanelInventory)inventory_.get(orderFromTop)).setBackground(image);
			else System.err.println("ERROR: Invalid inventory number");
		}catch(IOException e) {
			System.err.println("ERROR: Item file not found");
		}
	}
	
	//Draws all inventory items based on Laura's item ids
	public void drawItems(Vector<Integer> ids) {
		for (int i = 0; i < 6; ++i) {
			if (i < ids.size()) drawInventoryItem(ids.get(i), i+1);
			else drawInventoryItem(-1, i+1);
		}
	}

	public void drawBorder(int orderFromTop, boolean isSelected) {
		if(1<=orderFromTop || orderFromTop<=6)
			((JPanelInventory)inventory_.get(orderFromTop)).drawBorder(isSelected);
		else System.err.println("ERROR: Invalid order from top value");

	}
	
	public void drawCoordinateBackground(String id, int x, int y){
		try{
		BufferedImage image = ImageIO.read(new File(
				ImageLocations.getImageLocations().getFilePath(id)));
		((JPanelPoint)vertical.get(y).get(x)).setBackground(image);
		}catch (IOException ex){
			System.err.println("ERROR: Problem loading background image");
		}
	}
	
	public void drawStackCoordinate(String id, int x, int y){
		try{
		BufferedImage stack = ImageIO.read(new File(
				ImageLocations.getImageLocations().getFilePath(id)));
		BufferedImage back = (BufferedImage) ((JPanelWithBackground) vertical.get(y).get(x)).getImage();
		((JPanelPoint) vertical.get(y).get(x)).drawStacked(stack, back);
		}catch(Exception ex){
			System.err.println("ERROR: Problem with stacked image");
		}
	}
	
	public void lauraHealthbar(int health, int maxHealth){
		lhealthBar.setMaximum(maxHealth);
		lhealthBar.setValue(health);
		if(health > maxHealth/2) lhealthBar.setForeground(Color.GREEN);
		else if(health <= maxHealth/4) lhealthBar.setForeground(Color.RED);
		else lhealthBar.setForeground(Color.YELLOW);
		
		lhealthBar.setString(health + "/" + maxHealth);
		lhealthBar.setStringPainted(true);
	}
	
	public void creatureHealthbar(int health, int maxhealth){
		chealthBar.setMaximum(maxhealth);
		chealthBar.setValue(health);
		chealthBar.setVisible(true);
		if(health > maxhealth/2) chealthBar.setForeground(Color.GREEN);
		else if(health <= maxhealth/4) chealthBar.setForeground(Color.RED);
		else chealthBar.setForeground(Color.YELLOW);

		chealthBar.setString(health + "/" + maxhealth);
		chealthBar.setStringPainted(true);
	}

	@Override
	public void drawLauraStats(int health, int maxHealth, int strength, int defense, int speed) {
		lauraHealthbar(health, maxHealth);
		lauraStats(strength, defense, speed);	
	}
	
	private void lauraStats(int strength, int defense, int speed) {
		lstrength.setText("Attack: " + strength);
		lstrength.setEditable(false);
		lstrength.setOpaque(false);
		
		ldefense.setText("Defense: " + defense);
		ldefense.setEditable(false);
		ldefense.setOpaque(false);
		
		lspeed.setText("Moves left: " + speed);
		lspeed.setEditable(false);
		lspeed.setOpaque(false);
		
	}
	
	public void drawCreatureStats(String id, int health, int maxHealth, int strength, int defense, int speed){
		if(health<=0){
			chealthBar.setVisible(false);
			creatureImage("-1");
			creatureStats();
		}else{
			creatureHealthbar(health, maxHealth);
			creatureStats(id, strength, defense, speed);
			creatureImage(id);
		}
	}

	private void creatureImage(String id){
		try {
			cimage.setBackground(ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath(id))));
			if(id.equals("-1"))
				cimage.setBorder(null);
			else 
				cimage.setBorder(BorderFactory.createLineBorder(Color.black));
		} catch (IOException e) {
			System.err.println("ERROR: Problem with creatures image");
		}
	}
	
	private void creatureStats(){
		cname.setVisible(false);
		cstrength.setVisible(false);
		cdefense.setVisible(false);
		cspeed.setVisible(false);
	}
	
	private void creatureStats(String id, int strength, int defense, int speed) {
		cname.setText(id);
		cname.setEditable(false);
		cname.setFont(new Font("Tahoma", Font.BOLD, 14));
		cname.setOpaque(false);
		cname.setVisible(true);
		
		cstrength.setText("Strength: " + strength);
		cstrength.setEditable(false);
		cstrength.setOpaque(false);
		cstrength.setVisible(true);
		
		cdefense.setText("Defense: " + defense);
		cdefense.setEditable(false);
		cdefense.setOpaque(false);
		cdefense.setVisible(true);
		
		cspeed.setText("Speed: " + speed);
		cspeed.setEditable(false);
		cspeed.setOpaque(false);
		cspeed.setVisible(true);
	}
	
	public void drawItemStats(String id, String name, int healingvalue){
		creatureStats();
		chealthBar.setVisible(false);
		itemStats(name, healingvalue);
		creatureImage(id);
	}
	
	public void drawItemStats(String id, String name, int damage, boolean cutsWood){
		chealthBar.setVisible(false);
		itemStats(name, damage, cutsWood);
		creatureImage(id);
	}
	
	private void itemStats(String name, int damage, boolean cutsWood){
		cname.setText(name);
		cname.setEditable(false);
		cname.setFont(new Font("Tahoma", Font.BOLD, 14));
		cname.setOpaque(false);
		cname.setVisible(true);
		
		cstrength.setText("Damage: " + damage);
		cstrength.setEditable(false);
		cstrength.setOpaque(false);
		cstrength.setVisible(true);
		cdefense.setVisible(true);
		if (cutsWood){
			cdefense.setText("Cuts wood");
			cdefense.setEditable(false);
			cdefense.setOpaque(false);
		}
		else{
			cdefense.setVisible(false);
		}
		cspeed.setVisible(false);		
	}
	
	private void itemStats(String name, int healingvalue) {
		cname.setText(name);
		cname.setEditable(false);
		cname.setFont(new Font("Tahoma", Font.BOLD, 14));
		cname.setOpaque(false);
		cname.setVisible(true);
		
		cstrength.setText("Heals: " + healingvalue + " HP");
		cstrength.setEditable(false);
		cstrength.setOpaque(false);
		cstrength.setVisible(true);
		
		cdefense.setVisible(false);
		cspeed.setVisible(false);
	}
	
	public BufferedImage rotateToRight(BufferedImage inputImage, int rotationsToRight){
		if (rotationsToRight == 0){
			return inputImage;
		}else{
			--rotationsToRight;
			int width = inputImage.getWidth();
			int height = inputImage.getHeight();
			BufferedImage returnImage = new BufferedImage(height, width, inputImage.getType());
	
			for( int x = 0; x < width; x++ ) {
				for( int y = 0; y < height; y++ ) {
					returnImage.setRGB(height-y-1, x, inputImage.getRGB(x, y));
				}
			}
			return rotateToRight(returnImage, rotationsToRight);
		}
	}

	
	public void drawShotgunDetail(String id, int rotationsToRight, int x, int y) {
		try{
			BufferedImage stack = ImageIO.read(new File(
				ImageLocations.getImageLocations().getFilePath(id)));
			BufferedImage newstack = rotateToRight(stack, rotationsToRight);

			BufferedImage back = (BufferedImage) ((JPanelWithBackground) vertical.get(y).get(x)).getImage();
			((JPanelPoint) vertical.get(y).get(x)).drawStacked(newstack, back);
		}catch(Exception ex){
			System.err.println("ERROR: Problem with stacked image");
		}
	}
	
	@SuppressWarnings("serial")
	public void undrawShotgunDetail(final Vector<Coordinate> spread){
		int delay = 500;
		Timer timer = new Timer(delay, new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent ae) {
				for(int i = 0; i < spread.size(); ++i){
					if(spread.get(i) != null){
						spread.get(i).drawAll();
					}
				}
			}
		});
		timer.start();
	}
}
