package marjametsa_reloaded;

public class Weapon extends Item {
	
	private int damage_;
	private boolean cutsWood_;
	
	public Weapon(int id, String name, int damage, boolean cutsWood){
		super.id_ = id;
		super.name_ = name;
		damage_ = damage;
		cutsWood_ = cutsWood;
	}
	
	public int getDamage() {
		return damage_;
	}
	
	public boolean getCutsWood() {
		return cutsWood_;
	}
	
}
