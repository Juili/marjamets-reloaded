package marjametsa_reloaded;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class SaveGame {
	
	private BufferedWriter writer_;
	private Path path_;
	private File file_;
	
	public SaveGame() {
		writer_ = null;
		path_ = FileSystems.getDefault().getPath("configFiles", 
				"loadGame" + ".txt");
	}
	
	public void save(int openLevels) throws IOException {
        try {
            openFile();
            writeOpenLevels(openLevels);
            writeLauraStats();
            writeItems();
        }finally {
        	writer_.close();
        }
	}
	
	private void openFile() throws IOException {
    	file_ = path_.toFile();
        System.out.println("Saving to " + file_.getCanonicalPath());
        writer_ = new BufferedWriter(new FileWriter(file_));
        writeCommentLine("################LoadGame###############");
        writer_.newLine();
	}
	
	private void writeLauraStats() throws IOException {
		writeCommentLine("LAURA/ health, maxHealth, speed, strength, defense, name");
		writer_.write("L:" + Laura.getLaura().getHealth() + 
				"," + Laura.getLaura().getMaxHealth() + 
				"," + Laura.getLaura().getSpeed() + 
				"," + Laura.getLaura().getStrength() + 
				"," + Laura.getLaura().getDefense() + 
				"," + Laura.getLaura().getName());
		emptyLine();
	}
	
	private void writeItems() throws IOException {
		boolean needHeader = true;
		for (int i = 0; i < Laura.getLaura().getItemCount(); ++i) {
			if (Laura.getLaura().getItem(i) instanceof Potion) {
				if (needHeader) {
					needHeader = false;
					writeCommentLine("POTION/ id, name, healingValue");
				}
				writer_.write("P:" + Laura.getLaura().getItem(i).getId() +
						"," + Laura.getLaura().getItem(i).getName() + 
						"," + ((Potion)Laura.getLaura().getItem(i)).getHealingValue());
				writer_.newLine();
			}
		}
		needHeader = true;
		writer_.newLine();
		for (int i = 0; i < Laura.getLaura().getItemCount(); ++i) {
			if (Laura.getLaura().getItem(i) instanceof Weapon) {
				if (needHeader) {
					needHeader = false;
					writeCommentLine("WEAPON/ id, name, damage, cutsWood");
				}
				writer_.write("W:" + Laura.getLaura().getItem(i).getId() +
						"," + Laura.getLaura().getItem(i).getName() + 
						"," + ((Weapon)Laura.getLaura().getItem(i)).getDamage());
				if (((Weapon)Laura.getLaura().getItem(i)).getCutsWood()) 
					writer_.write(",1");
				else
					writer_.write(",0");
				writer_.newLine();
			}
		}
		writer_.newLine();
	}
	
	private void writeOpenLevels(int openLevels) throws IOException {
		writeCommentLine("OPEN LEVELS/ last opened level");
		writer_.write("O:" + openLevels);
		emptyLine();
	}
	
	private void writeCommentLine(String comment) throws IOException {
		writer_.write("#" + comment);
		writer_.newLine();
	}
	
	private void emptyLine() throws IOException {
		writer_.newLine();
		writer_.newLine();
	}
}
