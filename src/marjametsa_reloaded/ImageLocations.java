package marjametsa_reloaded;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class ImageLocations {
	
	private static ImageLocations instance_ = null;
	private Map<String, String> locations_;
	private Path filePath_;

	public static ImageLocations getImageLocations() {
		if (instance_ == null) {
			instance_ = new ImageLocations();
		}
		return instance_;
	}
	
	protected ImageLocations() {
		filePath_ = FileSystems.getDefault().getPath("configFiles", "imageLocations.txt");
		locations_ = new HashMap<String, String>();
		readFile();
	}
	
	private void readFile() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath_.toString()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				insertIntoMap(line);
			}
			reader.close();
		}catch(IOException e) {
			System.err.println("ERROR: " + filePath_ + " not found.");
		}
	}
	
	private void insertIntoMap(String line) {
		if (line.startsWith("#") || line.startsWith(" ") || line.isEmpty() || line == null ) return;
		String sublines[] = null;
		sublines = line.split(":");
		if (sublines == null) return;
		if (!locations_.containsKey(sublines[0])) {
			locations_.put(sublines[0], sublines[1]);
		}else {
			System.err.println("ERROR: Duplicate keys for imageLocations.");
		}
	}
	
	public String getFilePath(String key) {
		return locations_.get(key);
	}
}
