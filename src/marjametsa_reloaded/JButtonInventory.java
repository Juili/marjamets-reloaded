package marjametsa_reloaded;

import javax.swing.JButton;

public class JButtonInventory extends JButton {

	private static final long serialVersionUID = 1L;
	private String name_;
	
	public JButtonInventory(String name) {
		super(name);
		name_ = name;
	}
	
	public String getName() {
		return name_;
	}
}
