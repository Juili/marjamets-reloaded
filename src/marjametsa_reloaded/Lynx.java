package marjametsa_reloaded;

public class Lynx extends Creature {
	
	public Lynx() {
		
	}
	
	@Override
	public int getStrength() {
		if(Laura.getLaura().getItemInUse().getName().equals("Axe")){
			return strength_ * 2;
		}
		return strength_;
	}
}
