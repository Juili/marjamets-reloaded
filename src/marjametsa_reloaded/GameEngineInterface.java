package marjametsa_reloaded;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public interface GameEngineInterface {
	public void keyReleased(KeyEvent e);
	public void mouseClicked(MouseEvent e);
}
