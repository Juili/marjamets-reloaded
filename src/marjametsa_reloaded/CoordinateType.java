package marjametsa_reloaded;

public class CoordinateType extends Type {
	
	private GUICoordinateInterface coordGUI_;
	private int x_;
	private int y_;
	private String background_;
	
	public CoordinateType(GUICoordinateInterface coordGUI) {
		coordGUI_ = coordGUI;
	}
	
	public void setX(int x) {
		x_ = x;
	}
	
	public void setY(int y) {
		y_ = y;
	}
	
	public void setBackground(String bg) {
		background_ = bg;
	}
	
	public int getX() {
		return x_;
	}
	
	public int getY() {
		return y_;
	}
	
	public String getBackground() {
		return background_;
	}
	
	public GUICoordinateInterface getGUIInterface() {
		return coordGUI_;
	}
}
