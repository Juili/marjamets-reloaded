package marjametsa_reloaded;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

enum Direction {UP, RIGHT, DOWN, LEFT, NOWHERE}

public abstract class Creature {

	protected int health_;
	protected int maxHealth_;
	protected int speed_;
	protected int strength_;
	protected int defense_;
	protected int coordinateX_;
	protected int coordinateY_;
	protected int movesLeft_;
	protected String name_;

	public Direction randomDirection(){
		ArrayList<Direction> moveDirections = new ArrayList<Direction>();
		moveDirections.add(Direction.UP);
		moveDirections.add(Direction.RIGHT);
		moveDirections.add(Direction.DOWN);
		moveDirections.add(Direction.LEFT);
		
		long seed = System.nanoTime();
		Collections.shuffle(moveDirections, new Random(seed));
		
		return moveDirections.get(0);
	}
	
	public boolean move(Direction dir) {
		//System.out.println("Creature move");
		if (movesLeft_ == 0) {
			System.err.println("Error: No more moves left");
			return false;
		}
		switch (dir) {
		case UP:
			if (coordinateY_ == 0) {
				return true; //new Point(coordinateX_, coordinateY_);
			}
			--coordinateY_;
			//--movesLeft_;
			return true; //return new Point(coordinateX_, coordinateY_); 
		case RIGHT:
			if (coordinateX_ == 16) { //hard coded value, will be changed
				return true; //new Point(coordinateX_, coordinateY_);
			}
			++coordinateX_;
			//--movesLeft_;
			return true; //new Point(coordinateX_, coordinateY_); 
		case DOWN:
			if (coordinateY_ == 11) { //hard coded value, will be changed
				return true; //new Point(coordinateX_, coordinateY_);
			}
			++coordinateY_;
			//--movesLeft_;
			return true; //new Point(coordinateX_, coordinateY_);
		case LEFT:
			if (coordinateX_ == 0) {
				return true; //new Point(coordinateX_, coordinateY_);
			}
			--coordinateX_;
			//--movesLeft_;
			return true; //new Point(coordinateX_, coordinateY_);
		default: 
			System.err.println("Error: Invalid direction parameter");
			return false; //Error happened
		}
		
	}
	
	public void setX(int x) {
		coordinateX_ = x;
	}
	
	public void setY(int y) {
		coordinateY_ = y;
	}
	
	public int reduceHealth(int damage) {
		int reducedAmount = damage - defense_;
		if(reducedAmount < 0){
			reducedAmount = 0;
		}
		
		health_ -= reducedAmount;
		
		if (health_ < 0) {
			health_ = 0;
		}
		
		return health_;
	}
	
	public int getX() {
		return coordinateX_;
	}
	
	public int getY() {
		return coordinateY_;
	}
	
	public int getSpeed() {
		return speed_;
	}
	
	public int getHealth() {
		return health_;
	}
	
	public int getStrength() {
		return strength_;
	}
	
	public int getDefense() {
		return defense_;
	}
	
	public String getName() {
		return name_;
	}
	
	public boolean movesLeft(){
		if(movesLeft_ > 0){
			return true;
		}
		return false;
	}
	
	public int getMaxHealth() {
		return maxHealth_;
	}
	
	public void setHealth(int health) {
		health_ = health;
	}
	
	public void setSpeed(int speed) {
		speed_ = speed;
	}
	
	public void setStrength(int strength) {
		strength_ = strength;
	}
	
	public void setDefense(int defense) {
		defense_ = defense;
	}
	
	public void setName(String name) {
		name_ = name;
	}
	
	public void setMaxHealth(int maxHealth) {
		maxHealth_ = maxHealth;
	}
	
	public boolean act(LevelScheduler LS) {
		while (movesLeft_ > 0) {
			// get Laura's coordinates
			int LauraX = LS.getLauraX();
			int LauraY = LS.getLauraY();

			// Check if Laura is next to you
			if ((LauraX == coordinateX_ + 1 && LauraY == coordinateY_)
					|| (LauraX == coordinateX_ - 1 && LauraY == coordinateY_)
					|| (LauraY == coordinateY_ + 1 && LauraX == coordinateX_) 
					|| (LauraY == coordinateY_ - 1 && LauraX == coordinateX_)){
				// -----HIT LAURA-----
				Laura.getLaura().reduceHealth(strength_);
				--movesLeft_;
			}

			else {
				// -----MOVE TOWARDS LAURA-----
				// check directions
				ArrayList<Direction> moveDirections = new ArrayList<Direction>();

				if (LauraX > coordinateX_) {
					moveDirections.add(Direction.RIGHT);
				}

				if (LauraX < coordinateX_) {
					moveDirections.add(Direction.LEFT);
				}

				if (LauraY < coordinateY_) {
					moveDirections.add(Direction.UP);
				}

				if (LauraY > coordinateY_) {
					moveDirections.add(Direction.DOWN);
				}

				long seed = System.nanoTime();
				Collections.shuffle(moveDirections, new Random(seed));

				// check possible direction to move
				Direction moveDecision = Direction.NOWHERE;

				while (moveDirections.size() > 0) {
					if (moveDirections.get(0) == Direction.UP) {
						if (LS.ableToMove(coordinateX_, coordinateY_ - 1)) {
							moveDecision = Direction.UP;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.DOWN) {
						if (LS.ableToMove(coordinateX_, coordinateY_ + 1)) {
							moveDecision = Direction.DOWN;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.LEFT) {
						if (LS.ableToMove(coordinateX_ - 1, coordinateY_)) {
							moveDecision = Direction.LEFT;
							break;
						}
					}

					else if (moveDirections.get(0) == Direction.RIGHT) {
						if (LS.ableToMove(coordinateX_ + 1, coordinateY_)) {
							moveDecision = Direction.RIGHT;
							break;
						}
					}

					moveDirections.remove(0);
				}

				// If no possible directions to move, creature stays where it
				// is,
				// and function returns true
				if (moveDirections.size() == 0) {
					return true;
				}

				// remove creature from old coordinate
				if (!LS.removeCreature(coordinateX_, coordinateY_)) {
					return false;
				}

				// move to new direction
				this.move(moveDecision);

				// add creature to new coordinate
				LS.addCreature(coordinateX_, coordinateY_, this);

				// Reduce movesleft
				--movesLeft_;
			}
		}
		return true;
	}
	
	public void restoreMoves(){
		movesLeft_ = speed_;
		return;
	}
	
	public boolean isDead(){
		if(health_ <= 0){
			return true;
		}
		else return false;
	}
	
	protected Creature closestInRange(LevelScheduler LS, int range){
		int i = 0;
		Creature closest = null;
		double closestDistance = range;
		
		while(i < LS.countCreatures()){
			
			if(Math.sqrt(Math.pow((LS.getCreature(i).getX() - coordinateX_), 2) + 
					Math.pow((LS.getCreature(i).getY() - coordinateY_), 2)) <= closestDistance){
				
				closestDistance = Math.sqrt(Math.pow((LS.getCreature(i).getX() - coordinateX_), 2) + 
						Math.pow((LS.getCreature(i).getY() - coordinateY_), 2));
				
				if(! LS.getCreature(i).getName().equals("BEAR")){
					closest = LS.getCreature(i);
				}
			}
			++i;
		}
		
		return closest;
	}
}
