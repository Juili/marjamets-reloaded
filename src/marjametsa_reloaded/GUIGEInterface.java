package marjametsa_reloaded;

public interface GUIGEInterface {
	public void setRequestFocusInWindow();
	public void drawBorder(int orderFromTop, boolean isSelected);
	public void setVisible(boolean visible);
}
