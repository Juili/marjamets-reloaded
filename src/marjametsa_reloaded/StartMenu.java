package marjametsa_reloaded;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.SpringLayout;

public class StartMenu extends JFrame implements SMenuGEInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private ActionListener GEButton_;
	private String newGameButtonImageName_ = "newgamebutton.gif";
	private String loadGameButtonImageName_ = "loadgamebutton.gif";
	private String optionsButtonImageName_ = "optionsbutton.gif";
	
	public StartMenu(GameEngineInterface GE) {
		GEButton_ = (ActionListener)GE;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1024, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		BufferedImage image = null;
		try{
			System.out.println(ImageLocations.getImageLocations().getFilePath("MENU"));
			image = ImageIO.read(new File(
					ImageLocations.getImageLocations().getFilePath("MENU")));
		}catch (Exception ex){
			System.err.println("ERROR: Could not load background");
		}
		
		JPanel buttons = new JPanelWithBackground(image);
		buttons.setSize(1024, 768);
		contentPane.add(buttons);
		SpringLayout sl_buttons = new SpringLayout();
		buttons.setLayout(sl_buttons);
		
		final JButton NewGame = new JButtonLevel("New Game");
		addButtonImage(NewGame, newGameButtonImageName_);
		NewGame.addActionListener(GEButton_);
		
		final JButton LoadGame = new JButtonLevel("Load Game");
		addButtonImage(LoadGame, loadGameButtonImageName_);
		LoadGame.addActionListener(GEButton_);
		
		final JButton Options = new JButtonLevel("Options");
		addButtonImage(Options, optionsButtonImageName_);
		Options.addActionListener(GEButton_);

		sl_buttons.putConstraint(SpringLayout.NORTH, LoadGame,buttons.getHeight()/2-LoadGame.getIcon().getIconHeight()/2 , SpringLayout.NORTH, buttons);
		sl_buttons.putConstraint(SpringLayout.WEST, LoadGame, buttons.getWidth()/2-LoadGame.getIcon().getIconWidth()/2, SpringLayout.WEST, buttons);
		
		sl_buttons.putConstraint(SpringLayout.NORTH, NewGame, -70, SpringLayout.NORTH, LoadGame);
		sl_buttons.putConstraint(SpringLayout.WEST, NewGame, buttons.getWidth()/2-NewGame.getIcon().getIconWidth()/2, SpringLayout.WEST, buttons);
		
		sl_buttons.putConstraint(SpringLayout.NORTH, Options, 70, SpringLayout.NORTH, LoadGame);
		sl_buttons.putConstraint(SpringLayout.WEST, Options, buttons.getWidth()/2-NewGame.getIcon().getIconWidth()/2, SpringLayout.WEST, buttons);
		
		buttons.add(NewGame);
		buttons.add(LoadGame);
		buttons.add(Options);
				
		this.setVisible(true);
	}

	//add button images
	private void addButtonImage(JButton button, String image) {
		try {
			Image buttonImage = ImageIO.read(new File("pictures\\menu\\" + image));
			button.setIcon(new ImageIcon(buttonImage));
			button.setPreferredSize(new Dimension(buttonImage.getWidth(button), buttonImage.getHeight(button)));
		} catch (IOException ex) {
			System.err.println("ERROR: problem loading loading image " + image);
			ex.printStackTrace();
		}
	}
}
