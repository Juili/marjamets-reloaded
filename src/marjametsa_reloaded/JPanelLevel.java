package marjametsa_reloaded;

import java.awt.Image;

public class JPanelLevel extends JPanelWithBackground {

	private static final long serialVersionUID = 1L;
	private int lvlNum_;
	
	public JPanelLevel(Image i, int levelNum) {
		super(i);
		if (levelNum < 1 || levelNum > 7) {
			System.err.println("Error: Invalid level number");
		}else {
			lvlNum_ = levelNum;
		}
	}
	
	public int getLevelNumber() {
		return lvlNum_;
	}
}
