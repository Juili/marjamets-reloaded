package marjametsa_reloaded;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class JPanelPoint extends JPanelWithBackground{

	private static final long serialVersionUID = 1L;
	private Point location_;
 
	public Point getLocation(){
		return location_;
	}
	
	public void setLocation(int x, int y){
		location_.setLocation(x, y);
	}
	
	public JPanelPoint(Image i,int x, int y) {
		super(i);
		location_ = new Point(x,y);
		super.imageOrg_ = i; 
		super.image_ = i; 
		setOpaque(false); 
	}
	
	public void drawStacked(BufferedImage img1, BufferedImage img2){
		BufferedImage result = new BufferedImage(50,50,BufferedImage.TYPE_INT_RGB);
		Graphics g = result.getGraphics();
		g.drawImage(img2, 0, 0, null);
		g.drawImage(img1, 0, 0, null);
		
		super.image_ = result;
		setOpaque(false);
		repaint();
	}
}

